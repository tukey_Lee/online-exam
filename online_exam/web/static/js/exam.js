function chooseHandler(chooseId) {
    var adminId = document.getElementById("adminId").value;
    var examPaperId = document.getElementById("examPaperId").value;
    var args = chooseId.split("-");
    var subjectId = args[0];
    var chooseResult = args[1].trim();

    $.ajax({
        type:'GET',
        url:"/choose",
        data:{
            "adminId":adminId,
            "subjectId":subjectId,
            "examPaperId":examPaperId,
            "chooseResult":chooseResult,
        },
        dataType:'text',
        async:false,
        success: function(data) {
            if(data.trim()=="f") {
                alert("提交失败，未知异常");
            }
        },
        error: function(XMLHttpRequest,textStatus,errorThrown) {
            alert(XMLHttpRequest.status);
            alert(XMLHttpRequest.readyState);
            alert(textStatus);
        }
    });
}