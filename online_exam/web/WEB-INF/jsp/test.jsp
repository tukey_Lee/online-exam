<%--
  Created by IntelliJ IDEA.
  User: Tukey
  Date: 2022-05-14
  Time: 8:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/ceshi" method="post">
    <input name="examPlanId" value="1">
    <div>
        <label>考试班级</label>
        <input name="classId" >
    </div>
    <div>
        <label>考试科目</label>
        <input name="courseId" >
    </div>
    <div>
        <label>考试试卷</label>
        <input name="examPaperId" >
    </div>
    <div>
        <label>开考时间</label>
        <input name="beginTime" >
    </div>
    <button type="submit" id="add" class="btn btn-primary"  style="width: 100px" >提交</button>
</form>

</body>
<script type="text/javascript">
    jQuery(function($) {
        $('form').on('submit', function(event) {
            var $form = $(this);

            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function(data) {
                    alert(data);
                },
                error: function(XMLHttpRequest,textStatus,errorThrown) {
                    alert(XMLHttpRequest.status);
                    alert(XMLHttpRequest.readyState);
                    alert(textStatus);
                }
            });
            event.preventDefault();
        });
    });
</script>
</html>
