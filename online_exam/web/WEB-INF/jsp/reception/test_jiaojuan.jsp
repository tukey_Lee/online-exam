<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tukey
  Date: 2022-05-05
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>考试结束！本次考试统计数据如下：</h3>
<h2>单选题做对：${requestScope.radioRight}个,共${requestScope.radioScore}分</h2>
<h2>简答题做对：${requestScope.shortRight}个,共${requestScope.shortScore}分</h2>
<h2>总共做对：${requestScope.allRight}个,共${requestScope.score}分</h2>
<c:if test="${requestScope.score>90}">
    <h2>成绩优秀，继续努力！</h2>
</c:if>
<c:if test="${requestScope.score>=60 and requestScope.score <90}">
    <h2>成绩良好，继续努力！</h2>
</c:if>
<c:if test="${requestScope.score<60}">
    <h2>成绩不合格，继续努力！</h2>
</c:if>

</body>
</html>
