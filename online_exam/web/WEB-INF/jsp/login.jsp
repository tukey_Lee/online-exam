
<%--
  Created by IntelliJ IDEA.
  User: Tukey
  Date: 2022-03-19
  Time: 10:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>在线考试系统</title>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getServerPort()
                + path;
    %>
    <c:set var="path" value="<%=basePath%>"/>
    <link href='${path}/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>

    <link rel="stylesheet" href="../../static/css/style.css" type="text/css">
    <link href="../../static/css/popup-box.css" rel="stylesheet" type="text/css" media="all" />
<%--    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>--%>
<%--    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>--%>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Sign In And Sign Up Forms  Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script src="../../static/js/jquery.min.js"></script>
    <script src="../../static/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../static/js/modernizr.custom.53451.js"></script>
    <script type="text/javascript">

        var msg= '${msg}';
        if (msg){
            alert(msg);
            changeCpacha();
        }


        $(document).ready(function() {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });

        });


        function changeCpacha(){
            $("#cpacha-img").attr("src",'getCpaCha?v1=4&w1=140&h1=40&type=loginCpacha&t='+new Date().getTime());
        }

    </script>

</head>
<body>
<h1 >在线考试系统</h1>
<div class="w3layouts">
    <div class="signin-agile">
        <h2>登录
        </h2>
        <form action="index" method="post">
            <div>

                <input type="text" name="account" id="account" class="name" placeholder="账户" required="">
                <input type="password" name="password" id="password" class="password" placeholder="密码" required="">
                <input style="width: 35%;float: left" type="text" name="cpacha" id="cpacha" class="password" placeholder="验证码" required="">
                <img id="cpacha-img" title="点击切换验证码" src="getCpaCha?v1=4&w1=140&h1=40&type=loginCpacha"  style=" cursor: pointer; width:109px;height:48.8px;margin-left: 20px;margin-bottom: 20px " onclick="changeCpacha()" />

            </div>

            <ul>
                <li>
                    <input type="checkbox" id="brand1" value="">
                    <label for="brand1"><span></span>记住密码</label>
                </li>
            </ul>
            <a href="#">忘记密码?</a><br>
            <div class="clear"></div>
            <input type="submit" value="登录">
        </form>
    </div>
    <div class="signup-agileinfo">
        <h3>注册</h3>
        <div class="more">
           <p> 还没有账号？<a class="book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href="#small-dialog">点击注册</a></p>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div class="footer-w3l">
    <p class="agileinfo"> 登录和注册。exam -  <a href="http://www.baidu.cn/" target="_blank">tukey</a>
    </p>
</div>
<div class="pop-up">
    <div id="small-dialog" class="mfp-hide book-form">
        <h3>注册 </h3>
        <form action="enroll" method="post">
            <input type="text" name="account" placeholder="账户" required=""/>
            <input type="text" name="name" placeholder="姓名" required=""/>
            <input type="password" name="password" class="password" placeholder="密码" required=""/>
            <input name="className" type="text" list="typelist" placeholder="请选择班级"/>
            <datalist id="typelist">
                <c:forEach items="${requestScope.allClasses}}" var="c" varStatus="status">
                    　　<option>${requestScope.allClasses[status.count-1].className}</option>
                </c:forEach>
            </datalist>

            <input type="submit" value="点击注册">
        </form>
    </div>
</div>
</body>
</html>
