﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"  />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>在线考试系统</title>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getServerPort()
                + path;
    %>
    <c:set var="path" value="<%=basePath%>"/>
    <link href='${path }/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="${path }/static/assets/materialize/css/materialize.min.css" media="screen,projection" />
    <!-- Bootstrap Styles-->
    <link href="${path }/static/assets/css/bootstrap.css" rel="stylesheet" />
<%--    <link href="${path }/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" />--%>
    <!-- FontAwesome Styles-->
    <link href="${path}/static/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="${path }/static/assets/css/custom-styles.css" rel="stylesheet" />
    <%-- zeroModal   --%>
    <link rel="stylesheet" type="text/css" href="${path}/static/js/zeroModal/zeroModal.css">

    <script type="text/javascript">
        var msg= '${msg}';
        if (msgNo) {
            msg = "";
        }
        if (msg){
            alert(msg);
        }
    </script>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header" style="margin-left: 40px">
                <a class="navbar-brand" >
                    <strong href="index" >在线考试</strong>
                    <a class="navbar-brand" id="examCenter-link" href="willexams?classId=${sessionScope.admin.classInfo.classId}&adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试中心</a>
                    <a class="navbar-brand" id="mineCenter-link" href="history?adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试记录</a>
                </a>
            </div>

                <ul class="nav navbar-top-links navbar-right" style="margin-top:4px;margin-right: 30px" >
                    <c:if test="${sessionScope.admin != null }">
                    <li>
                        <a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1">
                            <i class="fa fa-user fa-fw"></i>
                            <b style="color:#808080">${sessionScope.admin.name}</b>
                            <i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                    </c:if>
                    <c:if test="${sessionScope.admin == null }">
                        <div class="btn-group" style="margin-top: 5px;margin-right: 20px">
                            <a class="btn " href="login">登录</a>
                        </div>
                    </c:if>
                </ul>


        </nav>
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="self/${sessionScope.admin.adminId}" id="self"><i class="fa fa-user fa-fw"></i> 个人信息 </a>
            </li>
            <li><a href="exit"><i class="fa fa-sign-out fa-fw"></i> 退出 </a>
            </li>
        </ul>


    </div>
    <div class="jumbotron" style="height: 750px;padding-top: 100px; padding-bottom: 0px; margin-bottom: 0px;">
        <div style="padding: 20px;margin-left: 230px">
            <img src="${path}/static/images/reception/home-bg.png" width="76%"/>
        </div>
    </div>


    <!-- jQuery Js -->
    <script src="../../static/assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="${path}/static/assets/materialize/js/materialize.min.js"></script>


    <script src="${path }/static/js/jquery.js"></script>
    <script src="${path }/static/js/bootstrap/bootstrap.min.js"></script>
    <script src="${path }/static/js/zeroModal/zeroModal.min.js"></script>
    <script type="text/javascript">

        $(function() {
            $("#examCenter-link, #mineCenter-link").click(function() {
                // 判断是否登录
                var user = '<%=session.getAttribute("admin")%>';
                if(user === "null") {
                    zeroModal.show({
                        title: "提示",
                        content: "登录后才能查看",
                        width : '200px',
                        height : '180px',
                        overlay : false,
                        ok : true,
                        onClosed : function() {
                            location.reload();
                        }
                    });
                    return false;
                }
            });

            $("#self").click(function() {
                var href = $(this).attr("href");
                zeroModal.show({
                    title: "个人信息查看",
                    content: "个人信息查看",
                    width : '400px',
                    height : '260px',
                    top : '185px',
                    left : '230px',
                    url: href,
                    overlay : false,
                    ok : true,
                    onClosed : function() {
                        location.reload();
                    }
                });
                return false;
            });
        });
    </script>


</body>

</html>