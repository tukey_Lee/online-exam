<%--
  Created by IntelliJ IDEA.
  User: Tukey
  Date: 2022-03-26
  Time: 01:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>学生信息</title>
	<%
    	String path = request.getContextPath();
	    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    %>
	<c:set var="path" value="<%=basePath %>"/>
	<link href="${path }/static/assets/css/bootstrap.css" rel="stylesheet" />
<%--	    <link href="${path }/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" />--%>
 	<link rel="stylesheet" type="text/css" href="${path }/static/js/zeroModal/zeroModal.css" />
 	<style type="text/css">
 		.title {
 			font-size: 12px;
 		}
 		.label-val {
 			cursor: pointer;
 		}
 	</style>
</head>
<body>
	<div style="width: 400px">
		<label style="display: none;" id="adminId">${sessionScope.admin.adminId }</label>
		<span class="title">姓&emsp;名: </span>
		<label class="label-val">${sessionScope.admin.name}</label>
		<br />
		<span class="title">登录账户: </span>
		<label class="label-val">${sessionScope.admin.account }</label>
		<br />
		<span class="title">登录密码: </span>
		<label class="label-val" val="${sessionScope.admin.password }" id="Pwd">******</label>

		<br />
		<span class="title">就读班级: </span>
		<label class="label-val">${sessionScope.admin.classInfo.className }</label>
		<br />
	</div>	

	<!-- js引入 -->
    <script src="${path }/static/js/jquery.js" type=""></script>
    <script src="${path }/static/js/zeroModal/zeroModal.min.js" type=""></script>
    <script src="${path }/static/js/bootstrap/bootstrap.min.js" type=""></script>
    <script type="text/javascript">
    	$(function() {
    		$("#Pwd").mouseover(function() {
    			$(this).text($(this).attr("val"));
    		}).mouseout(function() {
    			$(this).text("******");    			
    		}).click(function() {
    			$(this).replaceWith("<label for='resetPwd'></label><input onblur='resetPwd(this)' id='resetPwd' type='text' style='width: 100px'/>");
    		});
    	});

		function resetPwd(t) {
    		$.ajax({
    			type: "POST",
    			url: "reset/"+$(t).val()+"/"+$("#adminId").text(),
    			// url: "/reset",
    			success: function(data) {
    				if(data === "t") {
    					zeroModal.show({
    						title: "提示",
    						content: "密码已重置!",
    						width : '200px',
							height: '200px',
    						ok : true,
							overflow: false,
							opacity:0.8,
    					});
    					$("#resetPwd").replaceWith("<label class='label-val' id='Pwd'>••••••</label>");
    				} else {
    					alert("修改失败");
    				}
    			}
    		});
    	}
    </script>
</body>
</html>