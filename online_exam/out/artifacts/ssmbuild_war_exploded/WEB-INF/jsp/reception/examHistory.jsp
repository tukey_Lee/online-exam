<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>考试历史</title>
	<%
    	String path = request.getContextPath();
	    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    %>
	<c:set var="path" value="<%=basePath %>"/>
<%--	<link href='${path }/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>--%>
<%--	<link href="${path }/static/assets/css/bootstrap.css" rel="stylesheet" />--%>
<%--	&lt;%&ndash;    <link href="${path }/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" />&ndash;%&gt;--%>
<%-- 	<link rel="stylesheet" type="text/css" href="${path }/static/js/zeroModal/zeroModal.css" />--%>
	<link href='${path }/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="${path }/static/assets/materialize/css/materialize.min.css" media="screen,projection" />
	<!-- FontAwesome Styles-->
	<link href="${path}/static/assets/css/font-awesome.css" rel="stylesheet" />
	<!-- Custom Styles-->
	<link href="${path }/static/assets/css/custom-styles.css" rel="stylesheet" />
	<!-- Bootstrap Styles-->
	<link href="${path }/static/assets/css/bootstrap.css" rel="stylesheet" />
	<%--    <link href="${path }/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" />--%>
	<link rel="stylesheet" type="text/css" href="${path }/static/js/zeroModal/zeroModal.css" />
</head>
<body style="background-color: #EEEEEE;">
<div id="wrapper" >
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="navbar-header" style="margin-left: 40px">
			<a class="navbar-brand" >
				<a class="navbar-brand" href="index"><strong  >在线考试</strong></a>
				<a class="navbar-brand" id="examCenter-link" href="willexams?classId=${sessionScope.admin.classInfo.classId}&adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试中心</a>
				<a class="navbar-brand" id="mineCenter-link" href="history?adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试记录</a>
			</a>
		</div>
	</nav>
</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="row">
					<c:choose>
						<c:when test="${fn:length(sessionScope.examHistoryToStudent) > 0 }">
							<c:forEach items="${sessionScope.examHistoryToStudent }" var="ehp">
							<div class="card blue-grey darken-1 " style="width: 400px;border-radius: 25px ">
								<div class="card-content white-text">
								<span class="card-title">${ehp.examPaperName }</span>
								<p>
									题目数量: ${ehp.subjectNum } &emsp;&emsp;
									总分: ${ehp.examPaperScore } &emsp;&emsp;
									得分: ${ehp.examScore }
								</p>
								<p class="beginTime">考试时间:
									<c:if test="${ehp.beginTime == null }">暂未定义</c:if>
									<c:if test="${ehp.beginTime != null }">
										${ehp.beginTime }
									</c:if>
								</p>
								<div class="card-action">
									<p>
										<a class="btn btn-default btn-lg btn-block beginExam" href="../eyeHistory?historyId=${ehp.examPaperId }&adminId=${sessionScope.admin.adminId }">回顾试卷</a>
								</div>
								</div>
							</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							 <div class="jumbotron">
								 <h1>暂无历史考试记录</h1>
								 <p>学习，永无止境!</p>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- js引入 -->
    <script src="${path }/js/jquery.js"></script>
    <script src="${path }/js/bootstrap/bootstrap.min.js"></script>
    <script src="${path }/js/zeroModal/zeroModal.min.js"></script>
    <script type="text/javascript">
    	$(function() {
    		
    	});
    </script>
</body>
</html>