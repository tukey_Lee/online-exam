<?xml version="1.0" encoding="UTF-8" ?>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>考试中心</title>
	<%
    	String path = request.getContextPath();
	    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    %>
	<c:set var="path" value="<%=basePath %>"/>
	<link href='${path }/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="${path }/static/assets/materialize/css/materialize.min.css" media="screen,projection" />
	<!-- FontAwesome Styles-->
	<link href="${path}/static/assets/css/font-awesome.css" rel="stylesheet" />
	<!-- Custom Styles-->
	<link href="${path }/static/assets/css/custom-styles.css" rel="stylesheet" />
	<!-- Bootstrap Styles-->
	<link href="${path }/static/assets/css/bootstrap.css" rel="stylesheet" />
	<%--    <link href="${path }/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" />--%>
 	<link rel="stylesheet" type="text/css" href="${path }/static/js/zeroModal/zeroModal.css" />
</head>
<body style="background-color: #EEEEEE;">

<div id="wrapper">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="navbar-header" style="margin-left: 40px">
			<a class="navbar-brand" >
				<a class="navbar-brand" href="index"><strong  >在线考试</strong></a>
				<a class="navbar-brand" id="examCenter-link" href="willexams?classId=${sessionScope.admin.classInfo.classId}&adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试中心</a>
				<a class="navbar-brand" id="mineCenter-link" href="history?adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试记录</a>
			</a>
		</div>
	</nav>
</div>

	<div class="container" style="margin-top: 100px;">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="row" id="root-row">

					<c:choose>
						<c:when test="${fn:length(sessionScope.examPlans) > 0}">
							<c:forEach items="${sessionScope.examPlans}" var="ep">
								<div class="card blue-grey darken-1 " style="width: 400px;border-radius: 25px ">
									<div class="card-content white-text">
										<span class="card-title">${ep.examPaper.examPaperName }</span>
										<p>考试科目: ${ep.course.courseName } &emsp;&emsp; 考试时长: <span id="examPaperTime${ep.examPaper.examPaperId }">${ep.examPaper.examPaperTime }</span> 分钟</p>
										<p>题目数量: ${ep.examPaper.subjectNum } &emsp;&emsp; 总分: ${ep.examPaper.examPaperScore } &emsp;&emsp;
											难易程度:
											<c:if test="${ep.examPaper.examPaperEasy == 0 }">简单</c:if>
											<c:if test="${ep.examPaper.examPaperEasy == 1 }">普通</c:if>
											<c:if test="${ep.examPaper.examPaperEasy == 2 }">困难</c:if>
										</p>
										<p class="beginTime">开始时间: ${ep.beginTime}</p>
									</div>
									<div class="card-action">
										<p>
											<a class="btn btn-default btn-lg btn-block beginExam" id="${ep.examPaper.examPaperId }" href="begin?classId=${ep.classInfo.classId }&examPaperId=${ep.examPaper.examPaperId }&adminId=${sessionScope.admin.adminId }&examTime=${ep.examPaper.examPaperTime }&classId=${classId }&beginTime=${ep.beginTime }" >进入考试</a>
										</p>
									</div>
								</div>

							</c:forEach>
						</c:when>
						<c:otherwise>
							 <div class="jumbotron">
								 <h1>暂无待考信息</h1>
								 <p>请等待教师分配</p>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>

	<!-- js引入 -->
    <script src="${path }/static/js/jquery.js" type=""></script>
    <script src="${path }/static/js/bootstrap/bootstrap.min.js" type=""></script>
    <script src="${path }/static/js/zeroModal/zeroModal.min.js" type=""></script>
    <script type="text/javascript">

    	$(function() {
    		//进入考试控制
    		$(".beginExam").click(function() {
    			//获取开始时间
    			var abstractBeginTime = $(this).parent().prev(".beginTime").text();
    			var beginTime = abstractBeginTime.substring(abstractBeginTime.indexOf(":")+2);
    			//计算结束时间毫秒
    			var examPaperTime = parseInt($("#examPaperTime"+$(this).attr("id")).text()) * 60 * 1000;
    			//获取年月日
    			var date = beginTime.split(" ")[0];
    			var year = date.split("-")[0];
    			var month = parseInt(date.split("-")[1])-1;
    			var day = date.split("-")[2];
				//获取时分秒
				var time = beginTime.split(" ")[1];
				var hour = time.split(":")[0];
				var min = time.split(":")[1];
				var sec = (time.split(":")[2]).split(".")[0];
				//设置开始考试时间对象
				var beginDate = new Date();
				beginDate.setYear(year);
				beginDate.setMonth(month);
				beginDate.setDate(day);
				beginDate.setHours(hour);
				beginDate.setMinutes(min);
				beginDate.setSeconds(sec);
				//当前日期对象
				var nowDate = new Date();
				
				if(nowDate.getTime() < beginDate.getTime()) {
					zeroModal.show({
						title: "提示",
						content: "考试暂未开始, 请耐心等待!",
						width : '200px',
						height : '130px',
						overlay : true,
						ok : true,
						onClosed : function() {
							location.reload();
						}
					});
	    			return false;
				}
				if (nowDate.getTime() > (beginDate.getTime()+examPaperTime)) {
					zeroModal.show({
						title: "提示",
						content: "过期考试记录, 后台即将移除!",
						width : '200px',
						height : '130px',
						overlay : true,
						ok : true,
						onClosed : function() {
							location.reload();
						}
					});
	    			return false;
				}
    		});
    		
    		var divColMd4 = [];
    		var length = $(".row").find("div.col-md-4").length;
    		$("#root-row").find("div.col-md-4").each(function(i,e){
    			if(i > 2){
    				divColMd4.push($(e));
    				$(e).remove();
    			}
    			if(i == length-1){
    				var html = "";
    				var str ='';
    				var k=0;
    				for(;k<divColMd4.length;k++){
    					str +=  divColMd4[k].prop('outerHTML');
    					if((k+1)%3==0 && k!=0){
    						html += '<div class="row">' + str + '</div>';
    						str = '';
    					}
    				}
    				if((k)%3!=0){
    					html += '<div class="row">' + str + '</div>';;
    				}
    				$("#root-row").after(html);
    			}
    		})
    		
    		
    		
    	});
    </script>
</body>
</html>