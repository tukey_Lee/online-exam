<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>查看试卷</title>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    %>
    <c:set var="path" value="<%=basePath %>"/>
    <link href='${path }/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>

    <link href="${path}/static/beginTest/css/main.css" rel="stylesheet" type="text/css" />
    <link href="${path}/static/beginTest/css/test.css" rel="stylesheet" type="text/css" />
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css">
    <style type="text/css">
        .hasBeenAnswer {
            background: #5d9cec;
            color: #fff;
        }

        .reading h2 {
            width: 100%;
            margin: 20px 0 70px;
            text-align: center;
            line-height: 2;
            font-size: 20px;
            color: #59595b;
        }

        .reading h2 a {
            text-decoration: none;
            color: #59595b;
            font-size: 20px;
        }

        .reading h2 a:hover {
            color: #2183f1;
        }

        .title{
            width:900px;
            height:45px;
            font-family: Arial, sans-serif;
            font-size: 24px;
            color: #369;
            text-align: center;
            margin-top: 25px;
            margin-bottom: 45px
        }
    </style>
</head>
<body style="background-color: #EEEEEE;">

<div class="main">
    <!--nr start-->
    <div class="test_main">
        <div class="nr_left">
            <div class="test">
                <div class="title">
                    <h2>${sessionScope.examPaperById.examPaperName}</h2>&nbsp;&nbsp;<small>试卷总分：${sessionScope.examPaperById.examPaperScore}</small>&nbsp;&nbsp;<small>答题时间：${sessionScope.examPaperById.examPaperTime}分钟</small>
                </div>
                <form action="" method="post">

                    <div class="test_content">
                        <div class="test_content_title">
                            <h2>单选题</h2>
                            <p>
                                <span>共</span><i class="content_lit">${sessionScope.radioSub.size()}</i><span>题，</span><span>合计</span><i class="content_fs">${sessionScope.radioSub.size()*2}</i><span>分</span>
                            </p>
                        </div>
                    </div>
                    <div class="test_content_nr">
                        <c:set value="${sessionScope.radioSub}" var="radio_data"/>
                        <c:forEach items="#{radio_data}" var="c" varStatus="status">
                            <ul>
                                <li id="qu_0_${status.index}">
                                    <div class="test_content_nr_tt">
                                        <i>${status.index+1}</i><span>(${c.subjectScore}分)</span><font>${c.subjectName}</font><b class="icon iconfont"></b>
                                    </div>
                                    <div class="test_content_nr_main">
                                        <ul>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer${status.index+1}"
                                                       id="0_answer_${status.index+1}_option_1" />


                                                <label for="0_answer_${status.index+1}_option_1">
                                                    A.
                                                    <p class="ue" style="display: inline;">${c.optionA}</p>
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer${status.index+1}"
                                                       id="0_answer_${status.index+1}_option_2" />


                                                <label for="0_answer_${status.index+1}_option_2">
                                                    B.
                                                    <p class="ue" style="display: inline;">${c.optionB}</p>
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer${status.index+1}"
                                                       id="0_answer_${status.index+1}_option_3" />


                                                <label for="0_answer_${status.index+1}_option_3">
                                                    C.
                                                    <p class="ue" style="display: inline;">${c.optionC}</p>
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer${status.index+1}"
                                                       id="0_answer_${status.index+1}_option_4" />


                                                <label for="0_answer_${status.index+1}_option_4">
                                                    D.
                                                    <p class="ue" style="display: inline;">${c.optionD}</p>
                                                </label>
                                            </li>

                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </c:forEach>
                    </div>

                    <div class="test_content">
                        <div class="test_content_title">
                            <h2>简答题</h2>
                            <p>
                                <span>共</span><i class="content_lit">${sessionScope.shortSub.size()}</i><span>题，</span><span>合计</span><i class="content_fs">${sessionScope.shortSub.size()*10}</i><span>分</span>
                            </p>
                        </div>
                    </div>
                    <div class="test_content_nr">
                        <c:set value="${sessionScope.shortSub}" var="short_data"/>
                        <c:forEach items="#{short_data}" var="s" varStatus="status">
                            <ul>
                                <li id="qu_1_${status.index}">
                                    <div class="test_content_nr_tt">
                                        <i>${status.index+1}</i><span>(${s.subjectScore}分)</span><font>${s.subjectName}</font><b class="icon iconfont"></b>
                                    </div>
                                    <div class="test_content_nr_main">
                                        <ul>

                                            <li class="option">

                                                    <%--                                            <input type="checkbox" class="radioOrCheck" name="answer${status.index+1}"--%>
                                                    <%--                                                   id="1_answer_${status.index+1}_option_${status.index+1}" />--%>
                                                <form action="#" for="1_answer_${status.index+1}_option_${status.index+1}">
                                                        <%--                                                <label >--%>
                                                    <textarea rows="10" cols="105" onclick="Aa()">在这里输入........</textarea>
                                                        <%--                                                </label>--%>
                                                </form>
                                            </li>

                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </c:forEach>
                    </div>

                </form>
            </div>

        </div>
        <div class="nr_right">
            <div class="nr_rt_main">
                <div class="rt_nr1">
                    <div class="rt_nr1_title" >
                        <h1 style="width: 280px">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i> 答题卡
                        </h1>
                    </div>

                    <div class="rt_content">
                        <div class="rt_content_tt">
                            <h2>单选题</h2>
                            <c:set value="${sessionScope.radioSub.size()}" var="num1"/>
                            <p>
                                <span>共</span><i class="content_lit">${num1}</i><span>题</span>
                            </p>
                        </div>
                        <div class="rt_content_nr answerSheet">
                            <ul>
                                <c:set value="${sessionScope.radioSub}" var="radio_data"/>
                                <c:forEach items="#{radio_data}" var="c" varStatus="status">
                                    <li><a href="#qu_0_${status.index}">${status.index+1}</a></li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>

                    <div class="rt_content">
                        <div class="rt_content_tt">
                            <h2>简答题</h2>
                            <c:set value="${sessionScope.shortSub.size()}" var="num2"/>
                            <p>
                                <span>共</span><i class="content_lit">${num2}</i><span>题</span>
                            </p>
                        </div>
                        <div class="rt_content_nr answerSheet">
                            <ul>
                                <c:set value="${sessionScope.shortSub}" var="short_data"/>
                                <c:forEach items="#{short_data}" var="c" varStatus="status">
                                    <li><a href="#qu_1_${status.index+1}">${status.index+1}</a></li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!--nr end-->
    <div class="foot"></div>
</div>

<script src="http://cdn.bootstrapmb.com/jquery/jquery-1.11.1.min.js"></script>
<script src="${path}/static/beginTest/js/jquery.easy-pie-chart.js"></script>
<!--时间js-->
<script src="${path}/static/beginTest/time/jquery.countdown.js"></script>
<%--<script type="text/javascript">--%>
<%--    function Aa(){--%>

<%--        $('li.option form').mouseup(function () {--%>
<%--            debugger;--%>
<%--            var examId = $(this).closest('.test_content_nr_main').closest('li').attr('id'); /*得到题目ID*/--%>
<%--            var cardLi = $('a[href=#' + examId + ']'); /*根据题目ID找到对应答题卡*/--%>
<%--            /*设置已答题*/--%>
<%--            if (!cardLi.hasClass('hasBeenAnswer')) {--%>
<%--                cardLi.addClass('hasBeenAnswer');--%>
<%--            }--%>

<%--        });--%>
<%--    }--%>

<%--    window.jQuery(function ($) {--%>
<%--        "use strict";--%>

<%--        $('time').countDown({--%>
<%--            with_separators: false--%>
<%--        });--%>
<%--        $('.alt-1').countDown({--%>
<%--            css_class: 'countdown-alt-1'--%>
<%--        });--%>
<%--        $('.alt-2').countDown({--%>
<%--            css_class: 'countdown-alt-2'--%>
<%--        });--%>

<%--    });--%>


<%--    $(function () {--%>
<%--        $('li.option label').click(function () {--%>
<%--            debugger;--%>
<%--            var examId = $(this).closest('.test_content_nr_main').closest('li').attr('id'); /*得到题目ID*/--%>
<%--            var cardLi = $('a[href=#' + examId + ']'); /*根据题目ID找到对应答题卡*/--%>
<%--            /*设置已答题*/--%>
<%--            if (!cardLi.hasClass('hasBeenAnswer')) {--%>
<%--                cardLi.addClass('hasBeenAnswer');--%>
<%--            }--%>

<%--        });--%>
<%--    });--%>
<%--</script>--%>
</body>
</html>
