<%--
  Created by IntelliJ IDEA.
  User: Tukey
  Date: 2022-03-24
  Time: 19:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>后台管理</title>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    %>
    <c:set var="path" value="<%=basePath %>"/>
    <!-- Favicon -->
    <link href='${path }/static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>
    <!-- Bootstrap CSS -->
    <%--        <link rel="stylesheet" href="${path}/static/css/bootstrap/bootstrap.min.css">--%>
    <link href="${path }/static/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css">
    <%-- zeroModal   --%>
    <link rel="stylesheet" type="text/css" href="${path}/static/js/zeroModal/zeroModal.css">
    <!-- DataTables CSS -->
    <link rel="stylesheet" href="${path}/static/css/datatables/datatables.min.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="${path}/static/css/admin/style.css">

    <script type="text/javascript">
        var msg = '${msg}';
        if (msg)
            alert(msg);
    </script>
</head>

<!-- Main Wrapper -->
<div class="main-wrapper" >

    <!-- Header -->
    <div class="header">

        <!-- Logo -->
        <div class="header-left">
            <a href="admIndex" class="logo">
                <img src="${path}/static/images/admin/logo.png" alt="Logo" style="margin-left:40px">
            </a>
            <a href="admIndex" class="logo logo-small" >
                <img src="${path}/static/images/admin/logo-small.png" alt="Logo" width="30" height="30">
            </a>
        </div>
        <!-- /Logo -->


        <a href="javascript:void(0);" id="toggle_btn">
            <i class="fa fa-align-left"></i>
        </a>

        <!-- Header Right Menu -->
        <ul class="nav user-menu">

            <!-- User Menu -->
            <li class="nav-item dropdown has-arrow" style="margin-right: 10px">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" style="width: 200px">
                    <span class="user-img"><img class="rounded-circle" src="${path}/static/images/reception/photo.jpg" width="31" >
                        &nbsp;${sessionScope.admin.name}&nbsp;&nbsp;管理员&nbsp;
                    </span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="self/${sessionScope.admin.adminId}" id="self"><i class="fa fa-address-card"></i>&nbsp;个人信息</a>
                    <a class="dropdown-item" href="exit"><i class="fa fa-sign-out fa-fw"></i>退出</a>
                </div>
            </li>
            <!-- /User Menu -->

        </ul>
        <!-- /Header Right Menu -->

    </div>
    <!-- /Header -->

    <!-- Sidebar -->
    <div class="sidebar" id="sidebar" >
        <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                    <li class="active">
                        <a href="admIndex"><i class="fa fa-th-large"></i> <span>系统首页</span></a>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-user"></i> <span> 学生管理</span> <span class="fa fa-angle-down" style="margin-left: 140px"></span></a>
                        <ul>
                            <li><a href="students?classId=${sessionScope.admin.classInfo.classId}">学生列表</a></li>
                            <li><a href="addStudent">添加学生</a></li>
<%--                            <li><a href="editStudent">修改学生</a></li>--%>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-user-circle"></i> <span> 教师管理</span> <span class="fa fa-angle-down" style="margin-left: 140px"></span></a>
                        <ul>
                            <li><a href="teachers">教师列表</a></li>
                            <li><a href="addTeacher">添加教师</a></li>
<%--                            <li><a href="editTeacher">修改教师</a></li>--%>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-building"></i> <span> 班级管理</span> <span class="fa fa-angle-down" style="margin-left: 140px"></span></a>
                        <ul>
                            <li><a href="departments?classId=${sessionScope.admin.classInfo.classId}">班级列表</a></li>
                            <li><a href="addDepartment">添加班级</a></li>
<%--                            <li><a href="editDepartment">修改班级</a></li>--%>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-book" aria-hidden="true"></i> <span> 课程管理</span> <span class="fa fa-angle-down" style="margin-left: 140px"></span></a>
                        <ul>
                            <li><a href="courses">课程列表</a></li>
                            <li><a href="addCourse">添加课程</a></li>
<%--                            <li><a href="editSubject">修改课程 </a></li>--%>
                        </ul>
                    </li>
                    <li>
                        <a href="papers"><i class="fa fa-th-list"></i> <span>试卷管理</span></a>
                    </li>
                    <li>
                        <a href="questions"><i class="fa fa-th-list"></i> <span>试题管理</span></a>
                    </li>
                    <li>
                        <a href="timeTable"><i class="fa fa-table"></i> <span>考试安排管理</span></a>
                    </li>
                    <li>
                        <a href="historyTea"><i class="fa fa-history"></i> <span>以往考试信息</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- /Sidebar -->

    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-center">
                    <div class="col" style="margin-left: 21px;float: left">
                        <h3 class="page-title" style=" margin-bottom: 13px;">添加学生</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="admIndex">系统首页</a></li>
                            <li class="breadcrumb-item active">添加学生</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-sm-12">

                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="addStudent">
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="form-title"><span style="margin-left: 10px;font-size: larger">学生信息</span></h5>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>账号</label>
                                            <input type="text" class="form-control" name="account">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>密码</label>
                                            <input type="text" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>姓名</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>班级</label>
                                            <select class="form-control" name="className" id="className">
                                                <c:forEach items="${requestScope.allClasses}" var="c" varStatus="status">
                                                    　　<option>${requestScope.allClasses[status.count-1].className}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12" style="margin-left: 13px">
                                        <button type="submit" id="add" class="btn btn-primary"  style="width: 100px" >提交</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
            <!-- Footer -->
            <footer>
                <p>Copyright © 2022By <a href="http://www.baidu.com/">Tukey.</a></p>
            </footer>
            <!-- /Footer -->
        </div>
    </div>
    <!-- /Page Wrapper -->
</div>
<!-- /Main Wrapper -->
</div>
<!-- jQuery -->
<script src="${path}/static/js/jquery-3.5.1.min.js"></script>

<!-- Bootstrap Core JS -->
<script src="${path}/static/js/popper.min.js"></script>
<script src="${path}/static/js/bootstrap/bootstrap.min.js"></script>

<!-- Slimscroll JS -->
<script src="${path}/static/js/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom JS -->
<script  src="${path}/static/js/script.js"></script>

<script src="${path }/static/js/zeroModal/zeroModal.min.js"></script>

<%--datatables--%>
<script src="${path}/static/js/datatables/datatables.min.js" type=""></script>
<script type="text/javascript">
    $(function(){
        $("#self").click(function() {
            var href = $(this).attr("href");
            zeroModal.show({
                title: "个人信息查看",
                content: "个人信息查看",
                width : '350px',
                height : '280px',
                top : '185px',
                left : '230px',
                url: href,
                overlay : false,
                ok : true,
                onClosed : function() {
                    location.reload();
                }
            });
            return false;
        });

    });

    jQuery(function($) {
        $('form').on('submit', function(event) {
            var $form = $(this);

            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function(data) {
                    if (data === "success") {
                        window.location.replace("${path}/students?classId=${sessionScope.admin.classInfo.classId}");
                    }else {
                        alert(data);
                    }
                },
                error: function(data) {
                    alert("error！");
                }
            });
            event.preventDefault();
        });
    });

</script>

</html>