<%--
  Created by IntelliJ IDEA.
  User: Tukey
  Date: 2022-03-24
  Time: 19:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>后台管理系统</title>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    %>
    <c:set var="path" value="<%=basePath %>"/>
    <link href='${path}/static/images/admin/admin_index.png' rel='shortcut icon' type='image/x-icon' />
    <link href="${path}/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <!-- js引入 -->
    <script src="${path }/static/js/jquery.js" type=""></script>
    <script src="${path }/static/js/bootstrap/bootstrap.min.js"type=""></script>
</head>
<%--<c:if test="${sessionScope.loginTeacher == null }">--%>
<%--    <%response.sendRedirect("/admin/login.jsp"); %>--%>
<%--</c:if>--%>
<frameset rows="15%, *" frameborder="0">
    <frame src="${path }/home/admin/head.jsp" name="head" noresize="noresize" />
    <frameset cols="15%, *" frameborder="0">
        <frame src="${path }/home/admin/left.jsp" name="left" noresize="noresize" />
        <frameset rows="7%, *">
            <frame src="${path }/home/admin/nav.jsp" name="nav" noresize="noresize" />
            <frame src="${path }/home/admin/home.jsp" name="right" noresize="noresize" />
        </frameset>
    </frameset>
</frameset>
</html>