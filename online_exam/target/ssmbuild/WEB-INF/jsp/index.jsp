﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"  />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>在线考试系统</title>

 	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getServerPort()
                + path;
    %>
    <c:set var="path" value="<%=basePath%>"/>
    <link href='../../static/images/reception/index.png' rel='shortcut icon' type='image/x-icon'>
	<link rel="stylesheet" href="../../html/assets/materialize/css/materialize.min.css" media="screen,projection" />
    <!-- Bootstrap Styles-->
    <link href="../../html/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../../html/assets/css/custom-styles.css" rel="stylesheet" />
    <script type="text/javascript">
        var msg= '${msg}';
        if (msgNo) {
            msg = "";
        }
        if (msg){
            alert(msg);
        }
    </script>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <a class="navbar-brand" >
                    <strong href="index" >在线考试</strong>
                    <a class="navbar-brand" href="willexams?classId=${sessionScope.admin.classInfo.classId}&adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试中心</a>
                    <a class="navbar-brand" href="history?adminId=${sessionScope.admin.adminId}" adminid="${sessionScope.admin.adminId}">考试记录</a>
                </a>
                <c:if test="${sessionScope.admin != null }">
                    <ul class="nav navbar-top-links navbar-right">
                          <li style="margin-left: 1100px">
                              <a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1">
                                  <i class="fa fa-user fa-fw"></i>
                                  <b style="color:#808080">${sessionScope.admin.name }</b>
                               <i class="material-icons right">arrow_drop_down</i>
                              </a>
                          </li>
                    </ul>
                    </div>
                </c:if>
            <c:if test="${sessionScope.admin == null }">
                <div class="btn-group" style="margin-top: 5px;">
                    <a class="btn btn-default btn-sm" href="login">登录</a>
                </div>
            </c:if>
        </nav>

        <ul id="dropdown1" class="dropdown-content">
            <li><a href="self/${sessionScope.admin.account }" id="self"><i class="fa fa-user fa-fw"></i>个人信息</a>
            </li>
            <li><a href="exit"><i class="fa fa-sign-out fa-fw"></i> 退出</a>
            </li>
        </ul>

    </div>
    <div class="jumbotron" style="height: 630px;padding-top: 100px; padding-bottom: 0px; margin-bottom: 0px;">
        <div style="padding: 20px;">
            <img src="../../static/images/reception/home-bg.png" width="80%"/>
        </div>
    </div>
    <!-- jQuery Js -->
    <script src="../../html/assets/js/jquery-1.10.2.js"></script>

	<script src="../../html/assets/materialize/js/materialize.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $("#examCenter-link, #mineCenter-link").click(function() {
                // 判断是否登录
                var user = '<%=session.getAttribute("admin")%>';
                if(user === "null") {
                    zeroModal.show({
                        title: "提示",
                        content: "登录后才能查看",
                        width : '200px',
                        height : '130px',
                        overlay : false,
                        ok : true,
                        onClosed : function() {
                            location.reload();
                        }
                    });
                    return false;
                }
            });

            $("#self").click(function() {
                zeroModal.show({
                    title: "SelfInfo",
                    content: "个人信息查看",
                    width : '400px',
                    height : '200px',
                    top : '100px',
                    left : '430px',
                    url: "/self",
                    overlay : false,
                    ok : true,
                    onClosed : function() {
                        location.reload();
                    }
                });
                return false;
            });
        });
    </script>
 

</body>

</html>