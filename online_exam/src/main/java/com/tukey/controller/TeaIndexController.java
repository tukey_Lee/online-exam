package com.tukey.controller;

import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import com.tukey.pojo.CourseInfo;
import com.tukey.service.AdminService;
import com.tukey.service.ClassInfoService;
import com.tukey.service.CourseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
public class TeaIndexController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;
    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;

    @Autowired
    @Qualifier("courseInfoServiceImpl")
    private CourseInfoService courseInfoService;

    @GetMapping("/admIndex")
    public String getIndex() {
        return "admin/index";
    }

    @GetMapping("/students")
    public String getStudents(Integer classId, HttpServletRequest request) {
        List<Admin> studentsByClassId = adminService.getStudentsByClassId(classId);
        request.getSession().setAttribute("students", studentsByClassId);
        return "admin/students";
    }

    @GetMapping("/teachers")
    public String getTeachers(HttpServletRequest request) {
        List<Admin> allTeachers = adminService.getAllTeachers();
        request.getSession().setAttribute("teachers", allTeachers);
        return "admin/teachers";
    }

    @GetMapping("/departments")
    public String getDepartments(HttpServletRequest request) {
        HashMap<String, Integer> map = new HashMap<>();
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        List<Admin> allTeachers = adminService.getAllTeachers();
        for (ClassInfo classInfo:allClasses
             ) {
            int stuTotal = adminService.getStuTotal(classInfo.getClassId());
            map.put(classInfo.getClassName(),stuTotal);
        }
        request.getSession().setAttribute("allClasses", allClasses);
        request.getSession().setAttribute("allTeachers", allTeachers);
        request.getSession().setAttribute("stuTotal", map);
        return "admin/departments";
    }

    @GetMapping("/courses")
    public String getSubjects(HttpServletRequest request) {
        List<CourseInfo> courses = courseInfoService.getCourses();
        request.getSession().setAttribute("courses", courses);
        return "admin/courses";
    }

}
