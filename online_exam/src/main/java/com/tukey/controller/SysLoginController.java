package com.tukey.controller;

import com.tukey.pojo.ClassInfo;
import com.tukey.service.ClassInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 登录验证
 */
@Controller
public class SysLoginController {

    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;

    /**
     * 登录界面
     *
     * @param request
     * @return
     */
    @GetMapping("/login")
    public String login(HttpServletRequest request) {
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        return "login";
    }

}

