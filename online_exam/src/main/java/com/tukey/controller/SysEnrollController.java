package com.tukey.controller;

import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import com.tukey.service.AdminService;
import com.tukey.service.ClassInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 注册验证
 */
@Controller
public class SysEnrollController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;
    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;

    /**
     * 注册
     *
     * @param admin
     * @param request
     * @return
     */
    @PostMapping("enroll")
    public String enrollAct(Admin admin, String className, HttpServletRequest request) {
//        List<ClassInfo> classes = classInfoService.getClasses(classInfo);
        String msg = "位注册成功，请登陆";
        Admin dbAdmin = adminService.getAdminByAccountAndPwd(admin.getAccount());
        if (dbAdmin != null) {
            msg = "该账号已存在,请登录";
            return "login";
        }
        ClassInfo classInfo = classInfoService.getClassByName(className);
        admin.setClassInfo(classInfo);
        admin.setIdentity(2);
        int i = adminService.isAddAdmin(admin);
        request.setAttribute("msg", "第" + i + msg);
        return "login";
    }
}
