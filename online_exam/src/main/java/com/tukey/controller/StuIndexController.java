package com.tukey.controller;

import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import com.tukey.service.AdminService;
import com.tukey.service.ClassInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 学生端主页面
 */
@Controller
public class StuIndexController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;

    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;

    /**
     * 学生端主页面
     *
     * @return
     */
    @GetMapping("/index")
    public String getIndex() {
        return "index";
    }

    /**
     * 登录
     *
     * @param admin
     * @param cpacha
     * @param request
     * @return
     */
    @PostMapping("/index")
    public String loginAct(Admin admin, String cpacha, HttpServletRequest request) {

        String msg = "";

        if (admin == null) {
            msg = "请填写用户账号和密码!";
            request.setAttribute("msg", msg);
            return "login";
        }
        if (StringUtils.isEmpty(cpacha)){
            msg = "请填写验证码！";
            request.setAttribute("msg",msg);
            return "login";
        }
        if (StringUtils.isEmpty(admin.getAccount())) {
            msg = "请填写账户号！";
            request.setAttribute("msg", msg);
            return "login";
        }
        if (StringUtils.isEmpty(admin.getPassword())) {
            msg = "请填写密码！";
            request.setAttribute("msg", msg);
            return "login";
        }
        Object loginCpacha = request.getSession().getAttribute("loginCpacha");
        if (loginCpacha == null) {
            msg = "会话超时，请刷新页面！";
            request.setAttribute("msg", msg);
            return "login";
        }
        if (!cpacha.toUpperCase().equals(loginCpacha.toString().toUpperCase())) {
            msg = "验证码错误！";
            request.setAttribute("msg", msg);
            return "login";
        }

        Admin dbAdmin = adminService.getAdminByAccountAndPwd(admin.getAccount());

        if (dbAdmin == null) {
            msg = "用户不存在，请先注册";
            request.setAttribute("msg", msg);
            return "login";
        }
        if (admin.getPassword().equals(dbAdmin.getPassword())) {
            msg = "登录成功！";
            request.setAttribute("msg", msg);
            request.getSession().setAttribute("admin", dbAdmin);
            request.getSession().setAttribute("classInfo", dbAdmin.getClassInfo());
            if ((dbAdmin.getIdentity() == 1))
                return "admin/index";
            return "index";
        }

        msg = "登录异常，请重新登录！";
        request.setAttribute("msg", msg);
        return "login";
    }

    /**
     * 退出登录   无法删除session
     *
     * @return
     */
    @GetMapping("/exit")
    public String adminExit(HttpServletRequest request) {
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        request.getSession().removeAttribute("admin");
        return "login";
    }

    @GetMapping("/self/{adminId}")
    public String getSelfInfo(@PathVariable("adminId") Integer adminId, HttpServletRequest request) {

        Admin adminById = adminService.getAdminById(adminId);
        request.getSession().setAttribute("admin", adminById);
        return "reception/self";
    }

    @PostMapping("/reset/{pwd}/{adminId}")
    public void getNewPwd(
            @PathVariable("adminId") Integer adminId,
            @PathVariable("pwd") String pwd, HttpServletResponse response) throws IOException {
        Admin adminById = adminService.getAdminById(adminId);
        adminById.setPassword(pwd);
        int row = adminService.isResetPwdWithAdmin(adminById);
//        request.getSession().setAttribute("admin",adminById);
        if (row > 0) {
            response.getWriter().print("t");
        } else {
            response.getWriter().print("f");
        }

    }

}
