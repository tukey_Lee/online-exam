package com.tukey.controller;

import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import com.tukey.pojo.CourseInfo;
import com.tukey.service.AdminService;
import com.tukey.service.ClassInfoService;
import com.tukey.service.CourseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class InforController {
    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;

    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;

    @Autowired
    @Qualifier("courseInfoServiceImpl")
    private CourseInfoService courseInfoService;

    /**
     * get: 进入添加student界面 springmvc
     * post: 提交添加表单 jquery下的ajax
     *
     * @param request
     * @return
     */
    @GetMapping("/addStudent")
    public String addStudent(HttpServletRequest request) {
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        return "admin/addStudent";
    }

    @PostMapping("/addStudent")
    public void addStudent_btn(Admin admin, String className, HttpServletResponse response) throws IOException {

        String msg = "success";
        Admin dbAdmin = adminService.getAdminByAccountAndPwd(admin.getAccount());
        if (dbAdmin != null) {
            msg = "error";
            response.getWriter().print(msg);
        }
        ClassInfo classInfo = classInfoService.getClassByName(className);
        admin.setClassInfo(classInfo);
        admin.setIdentity(2);
        int addAdmin = adminService.isAddAdmin(admin);
        response.getWriter().print(msg);
    }

    /**
     * get: 进入修改student界面 springmvc
     * post: 提交修改表单 jquery下的ajax
     *
     * @param adminId
     * @param request
     * @return
     */
    @GetMapping("/editStudent")
    public String editStudent(Integer adminId, HttpServletRequest request) {
        Admin adminById = adminService.getAdminById(adminId);
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        request.setAttribute("studentById", adminById);
        return "admin/editStudent";
    }

    @PostMapping("/editStudent")
    public void editStudent_btn(Admin admin, String className, HttpServletResponse response) throws IOException {
        String msg = "error";
        ClassInfo classByName = classInfoService.getClassByName(className);
        admin.setIdentity(2);
        admin.setClassInfo(classByName);
        int updateAdmin = adminService.isUpdateAdmin(admin);
        if (updateAdmin > 0) {
            msg = "success";
            response.getWriter().print(msg);
        } else {
            response.getWriter().print(msg);
        }
    }

    /**
     * 删除学生 response 输出重定向
     *
     * @param adminId
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/deleStudent")
    public void deleStudent(Integer adminId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String msg = "success";
        Admin adminById = adminService.getAdminById(adminId);
        Integer classId = adminById.getClassInfo().getClassId();
        int delAdmin = adminService.isDelAdmin(adminId);
        if (delAdmin <= 0) {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        response.sendRedirect("http://localhost:8848/students?classId=" + classId);
    }


    /**
     * get: 进入添加teacher界面 springmvc
     * post: 提交添加表单 jquery下的ajax
     *
     * @param request
     * @return
     */
    @GetMapping("/addTeacher")
    public String addTeacher(HttpServletRequest request) {
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        return "admin/addTeacher";
    }

    @PostMapping("/addTeacher")
    public void addTeacher_btn(Admin admin, String className, HttpServletResponse response) throws IOException {
        String msg = "success";
        Admin dbAdmin = adminService.getAdminByAccountAndPwd(admin.getAccount());
        if (dbAdmin != null) {
            msg = "error";
            response.getWriter().print(msg);
        }
        ClassInfo classInfo = classInfoService.getClassByName(className);
        admin.setClassInfo(classInfo);
        admin.setIdentity(1);
        int addAdmin = adminService.isAddAdmin(admin);
        response.getWriter().print(msg);
    }

    /**
     * get: 进入修改teacher界面 springmvc
     * post: 提交修改表单 jquery下的ajax
     *
     * @param adminId
     * @param request
     * @return
     */
    @GetMapping("/editTeacher")
    public String editTeacher(Integer adminId, HttpServletRequest request) {
        Admin adminById = adminService.getAdminById(adminId);
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        request.setAttribute("teacherById", adminById);
        return "admin/editTeacher";
    }

    @PostMapping("/editTeacher")
    public void editTeacher_btn(Admin admin, String className, HttpServletResponse response) throws IOException {
        String msg = "error";
        ClassInfo classByName = classInfoService.getClassByName(className);
        admin.setIdentity(1);
        admin.setClassInfo(classByName);
        int updateAdmin = adminService.isUpdateAdmin(admin);
        if (updateAdmin > 0) {
            msg = "success";
            response.getWriter().print(msg);
        } else {
            response.getWriter().print(msg);
        }
    }

    /**
     * 删除老师 response 输出重定向
     *
     * @param adminId
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/deleTeacher")
    public void deleTeacher(Integer adminId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String msg = "success";
        Admin adminById = adminService.getAdminById(adminId);
        Integer classId = adminById.getClassInfo().getClassId();
        int delAdmin = adminService.isDelAdmin(adminId);
        if (delAdmin <= 0) {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        response.sendRedirect("http://localhost:8848/teachers");
    }

    /**
     * get: 进入添加department界面 springmvc
     * post: 提交添加表单 jquery下的ajax
     * @Param request
     * @return
     */
    @GetMapping("/addDepartment")
    public String addDepartment(HttpServletRequest request) {

        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("newClassId",allClasses.size());
        return "admin/addDepartment";
    }

    @PostMapping("/addDepartment")
    public void addDepartment_btn(ClassInfo classInfo, HttpServletResponse response) throws IOException {
        String msg = "success";
        ClassInfo classByName = classInfoService.getClassByName(classInfo.getClassName());
        if (classByName != null) {
            msg = "error";
            response.getWriter().print(msg);
        }
        int addClass = classInfoService.isAddClass(classInfo);
        response.getWriter().print(msg);
    }

    /**
     * get: 进入修改department界面 springmvc
     * post: 提交修改表单 jquery下的ajax
     *
     * @param classId
     * @param request
     * @return
     */
    @GetMapping("/editDepartment")
    public String editDepartment(Integer classId, HttpServletRequest request) {
        ClassInfo classById = classInfoService.getClassById(classId);
        request.setAttribute("classById", classById);
        return "admin/editDepartment";
    }

    @PostMapping("/editDepartment")
    public void editDepartment_btn(ClassInfo classInfo, HttpServletResponse response) throws IOException {
        String msg = "error";
        int updateClass = classInfoService.isUpdateClass(classInfo);
        if (updateClass > 0) {
            msg = "success";
            response.getWriter().print(msg);
        } else {
            response.getWriter().print(msg);
        }
    }

    /**
     * 删除班级 response 输出重定向
     * @param classId
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/deleDepartment")
    public void deleDepartment(Integer classId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String msg = "success";
        int delClass = classInfoService.isDelClass(classId);
        if (delClass <= 0) {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        response.sendRedirect("http://localhost:8848/teachers");

    }


    /**
     * get: 进入添加course界面 springmvc
     * post: 提交添加表单 jquery下的ajax
     *
     * @param request
     * @return
     */
    @GetMapping("/addCourse")
    public String addCourse(HttpServletRequest request) {
        List<CourseInfo> courses = courseInfoService.getCourses();
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        request.setAttribute("newCourseId", courses.size());
        return "admin/addCourse";
    }
    @PostMapping("/addCourse")
    public void addCourse_btn(CourseInfo courseInfo, String division_name, String className, HttpServletResponse response) throws IOException {
        String msg = "success";
        CourseInfo courseById = courseInfoService.getCourseById(courseInfo.getCourseId());
        if (courseById != null) {
            msg = "error";
            response.getWriter().print(msg);
        }
        if ("易".equals(division_name)) {
            courseInfo.setDivision(1);
        }
        if ("难".equals(division_name)) {
            courseInfo.setDivision(2);
        }
        ClassInfo classByName = classInfoService.getClassByName(className);
        courseInfo.setClassInfo(classByName);
        int addCourse = courseInfoService.isAddCourse(courseInfo);
        response.getWriter().print(msg);
    }

    /**
     * get: 进入修改course界面 springmvc
     * post: 提交修改表单 jquery下的ajax
     * @param courseId
     * @param request
     * @return
     */
    @GetMapping("/editCourse")
    public String editCourse(Integer courseId, HttpServletRequest request) {
        CourseInfo courseById = courseInfoService.getCourseById(courseId);
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("allClasses", allClasses);
        request.setAttribute("courseById", courseById);
        return "admin/editCourse";
    }
    @PostMapping("/editCourse")
    public void editCourse_btn(Integer courseId, String courseName, String division_name, String className, HttpServletResponse response) throws IOException {
        String msg = "error";
        ClassInfo classByName = classInfoService.getClassByName(className);
        CourseInfo courseById = courseInfoService.getCourseById(courseId);
        if ("易".equals(division_name)) {
            courseById.setDivision(1);
        }
        if ("难".equals(division_name)) {
            courseById.setDivision(2);
        }
        courseById.setCourseName(courseName);
        courseById.setClassInfo(classByName);
        int updateCourse = courseInfoService.isUpdateCourse(courseById);
        if (updateCourse > 0) {
            msg = "success";
            response.getWriter().print(msg);
        } else {
            response.getWriter().print(msg);
        }
    }
    /**
     * 删除班级 response 输出重定向
     * @param courseId
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/deleCourse")
    public void deleCourse(Integer courseId, HttpServletRequest request,HttpServletResponse response) throws IOException {
        String msg = "success";
        int delCourse = courseInfoService.isDelCourse(courseId);
        if (delCourse <= 0) {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        response.sendRedirect("http://localhost:8848/teachers");
    }

}
