package com.tukey.controller;

import com.tukey.util.CpachaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * 本系统所有的验证码均采用此方法
 */
@Controller
public class SysCpachaController {
    /**
     * @param vcodeLen
     * @param width
     * @param height
     * @param cpachaType : 用来区别验证码的类型，传入字符串
     * @param request
     * @param response
     */
    @GetMapping("/getCpaCha")
    public void generateCpaCha(
            @RequestParam(name = "v1", required = false, defaultValue = "4") Integer vcodeLen,
            @RequestParam(name = "w1", required = false, defaultValue = "100") Integer width,
            @RequestParam(name = "h1", required = false, defaultValue = "30") Integer height,
            @RequestParam(name = "type", required = true, defaultValue = "loginCpacha") String cpachaType,
            HttpServletRequest request,
            HttpServletResponse response) {

        CpachaUtil cpachaUtil = new CpachaUtil(vcodeLen, width, height);
        String generatorVCode = cpachaUtil.generatorVCode();
        request.getSession().setAttribute(cpachaType, generatorVCode);
        BufferedImage generatorRotateVCodeImage = cpachaUtil.generatorRotateVCodeImage(generatorVCode, true);
        try {
            ImageIO.write(generatorRotateVCodeImage, "jpg", response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
