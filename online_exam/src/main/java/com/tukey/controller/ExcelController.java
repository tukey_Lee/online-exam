package com.tukey.controller;

import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import com.tukey.service.AdminService;
import com.tukey.service.ClassInfoService;
import com.tukey.util.AdminInfoImportUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ExcelController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;

    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;


    /**
     * student excel导入导出
     * @param fileName
     * @param response
     * @throws IOException
     */
    @PostMapping("/addStudentForExcel")
    public void addStudent_excel(String fileName, HttpServletResponse response) throws IOException {
        String msg = "success";
        String url = "C://Users//Tukey//Desktop//test//";
        ArrayList admins = new AdminInfoImportUtil().getAdminInfo(url + fileName);
        if (admins == null){
            msg = "error";
            response.getWriter().print(msg);
        }
        for (Object admin : admins
        ) {
            Admin adminInfo = (Admin) admin;
            Admin dbAdmin = adminService.getAdminByAccountAndPwd(adminInfo.getAccount());
            if (dbAdmin != null) {
                continue;
            }
            ClassInfo classByName = classInfoService.getClassByName(adminInfo.getClassInfo().getClassName());
            adminInfo.setIdentity(2);
            adminInfo.setClassInfo(classByName);
            int addAdmin = adminService.isAddAdmin(adminInfo);
        }
        response.getWriter().print(msg);
    }
    @RequestMapping("/outStudentExcel")
    private void getStuExcel(HttpServletResponse response) throws Exception {
        SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory,
        Sheet sh = wb.createSheet();
        // 这个是业务数据
        List<Admin> admins = adminService.getAdmins();
//        List<TbClass> tmps = classService.getAllClass();
        String[] titles = {"账号", "姓名", "密码", "班级"};
        Row row = sh.createRow(0);
        // 第一行设置标题
        for (int i = 0; i < titles.length; i++) {
            String title = titles[i];
            Cell cell1 = row.createCell(i);
            cell1.setCellValue(title);
        }

        // 导出数据
        for (int rowNum = 0; rowNum < admins.size(); rowNum++) {

            Row rowData = sh.createRow(rowNum + 1);
            // TbClass 这个是我的业务类，这个是根据业务来进行填写数据
            Admin admin = admins.get(rowNum);
            if (admin.getIdentity() == 1) {
                continue;
            }
            // 第一列
            Cell cellDataA = rowData.createCell(0);
            cellDataA.setCellValue(admin.getAccount());
            // 第二列
            Cell cellDataB = rowData.createCell(1);
            cellDataB.setCellValue(admin.getName());
            // 第三列
            Cell cellDataC = rowData.createCell(2);
            cellDataC.setCellValue(admin.getPassword());
            // 第四列
            Cell cellDataD = rowData.createCell(3);
            cellDataD.setCellValue(admin.getClassInfo().getClassName());
        }
        String fileName = "文件名称.xlsx";
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        wb.write(response.getOutputStream());
        wb.close();
    }

    /**
     * teacher excel导入导出
     * @param fileName
     * @param response
     * @throws IOException
     */
    @PostMapping("/addTeacherForExcel")
    public void addTeacher_excel(String fileName, HttpServletResponse response) throws IOException {
        String msg = "success";
        String url = "C://Users//Tukey//Desktop//test//";
        ArrayList admins = new AdminInfoImportUtil().getAdminInfo(url + fileName);
        if (admins == null){
            msg = "error";
            response.getWriter().print(msg);
        }
        for (Object admin : admins
        ) {
            Admin adminInfo = (Admin) admin;
            Admin dbAdmin = adminService.getAdminByAccountAndPwd(adminInfo.getAccount());
            if (dbAdmin != null) {
                continue;
            }
            ClassInfo classByName = classInfoService.getClassByName(adminInfo.getClassInfo().getClassName());
            adminInfo.setIdentity(1);
            adminInfo.setClassInfo(classByName);
            int addAdmin = adminService.isAddAdmin(adminInfo);
            System.out.println(addAdmin);
        }
        response.getWriter().print(msg);
    }
    @RequestMapping("/outTeacherExcel")
    private void getTeaExcel(HttpServletResponse response) throws Exception {
        SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory,
        Sheet sh = wb.createSheet();
        // 这个是业务数据
        List<Admin> admins = adminService.getAdmins();
//        List<TbClass> tmps = classService.getAllClass();
        String[] titles = {"账号", "姓名", "密码", "班级"};
        Row row = sh.createRow(0);
        // 第一行设置标题
        for (int i = 0; i < titles.length; i++) {
            String title = titles[i];
            Cell cell1 = row.createCell(i);
            cell1.setCellValue(title);
        }

        // 导出数据
        for (int rowNum = 0; rowNum < admins.size(); rowNum++) {

            Row rowData = sh.createRow(rowNum + 1);
            // TbClass 这个是我的业务类，这个是根据业务来进行填写数据
            Admin admin = admins.get(rowNum);
            if (admin.getIdentity() == 2) {
                continue;
            }
            // 第一列
            Cell cellDataA = rowData.createCell(0);
            cellDataA.setCellValue(admin.getAccount());
            // 第二列
            Cell cellDataB = rowData.createCell(1);
            cellDataB.setCellValue(admin.getName());
            // 第三列
            Cell cellDataC = rowData.createCell(2);
            cellDataC.setCellValue(admin.getPassword());
            // 第四列
            Cell cellDataD = rowData.createCell(3);
            cellDataD.setCellValue(admin.getClassInfo().getClassName());
        }
        String fileName = "文件名称.xlsx";
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        wb.write(response.getOutputStream());
        wb.close();
    }

    @PostMapping("/addDepartmentForExcel")
    public void addDepartment_excel(String fileName, HttpServletResponse response) throws IOException {
        String msg = "success";
//        String url = "C://Users//Tukey//Desktop//test//";
//        ArrayList admins = new AdminInfoImportUtil().getAdminInfo(url + fileName);
//        for (Object admin : admins
//        ) {
//            Admin adminInfo = (Admin) admin;
//            Admin dbAdmin = adminService.getAdminByAccountAndPwd(adminInfo.getAccount());
//            if (dbAdmin != null) {
//                msg = "error";
//                response.getWriter().print(msg);
//                return;
//            }
//            ClassInfo classByName = classInfoService.getClassByName(adminInfo.getClassInfo().getClassName());
//            adminInfo.setIdentity(1);
//            adminInfo.setClassInfo(classByName);
//            int addAdmin = adminService.isAddAdmin(adminInfo);
//            System.out.println(addAdmin);
//        }
        response.getWriter().print(msg);
    }
    @RequestMapping("/outDepartmentExcel")
    private void getDepExcel(HttpServletResponse response) throws Exception {
//        SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory,
//        Sheet sh = wb.createSheet();
//        // 这个是业务数据
//        List<Admin> admins = adminService.getAdmins();
////        List<TbClass> tmps = classService.getAllClass();
//        String[] titles = {"账号", "姓名", "密码", "班级"};
//        Row row = sh.createRow(0);
//        // 第一行设置标题
//        for (int i = 0; i < titles.length; i++) {
//            String title = titles[i];
//            Cell cell1 = row.createCell(i);
//            cell1.setCellValue(title);
//        }
//
//        // 导出数据
//        for (int rowNum = 0; rowNum < admins.size(); rowNum++) {
//
//            Row rowData = sh.createRow(rowNum + 1);
//            // TbClass 这个是我的业务类，这个是根据业务来进行填写数据
//            Admin admin = admins.get(rowNum);
//            if (admin.getIdentity() == 2) {
//                break;
//            }
//            // 第一列
//            Cell cellDataA = rowData.createCell(0);
//            cellDataA.setCellValue(admin.getAccount());
//            // 第二列
//            Cell cellDataB = rowData.createCell(1);
//            cellDataB.setCellValue(admin.getName());
//            // 第三列
//            Cell cellDataC = rowData.createCell(2);
//            cellDataC.setCellValue(admin.getPassword());
//            // 第四列
//            Cell cellDataD = rowData.createCell(3);
//            cellDataD.setCellValue(admin.getClassInfo().getClassName());
//        }
//        String fileName = "文件名称.xlsx";
//        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
//        wb.write(response.getOutputStream());
//        wb.close();
    }
    //    @PostMapping("/addCourseForExcel")
    //    public void addCourse_excel(String fileName, HttpServletResponse response) throws IOException{}
    //    @RequestMapping("/outCourseExcel")
    //    private void getCouExcel(HttpServletResponse response) throws Exception{}
}
