package com.tukey.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import com.tukey.pojo.ExamPlanInfo;
import com.tukey.service.ExamPlanInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class test {

    @Autowired
    @Qualifier("examPlanInfoServiceImpl")
    private ExamPlanInfoService examPlanInfoService;

    @GetMapping("/ceshi")
    public String ceShi() throws IOException {
        return "test";
    }
    @PostMapping("/ceshi")
    public void ceShi(ExamPlanInfo examPlanInfo,
                      Integer courseId,
                      Integer classId,
                      Integer examPaperId,
                      HttpServletResponse response) throws IOException {
        System.out.println(examPlanInfo);
        System.out.println(courseId);
        System.out.println(classId);
        System.out.println(examPaperId);

    }
//        response.getWriter().print("success");

}
