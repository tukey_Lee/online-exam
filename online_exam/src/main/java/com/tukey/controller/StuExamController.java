package com.tukey.controller;

import com.tukey.pojo.*;
import com.tukey.service.*;
import com.tukey.util.SimilarityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;


/**
 * 学生端考试
 */
@Controller
public class StuExamController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;

    @Autowired
    @Qualifier("examPlanInfoServiceImpl")
    private ExamPlanInfoService examPlanInfoService;

    @Autowired
    @Qualifier("examHistoryInfoServiceImpl")
    private ExamHistoryInfoService examHistoryInfoService;

    @Autowired
    @Qualifier("examPaperInfoServiceImpl")
    private ExamPaperInfoService examPaperInfoService;

    @Autowired
    @Qualifier("examSubjectMiddleInfoServiceImpl")
    private ExamSubjectMiddleInfoService subjectMiddleInfoService;

    @Autowired
    @Qualifier("examChooseInfoServiceImpl")
    private ExamChooseInfoService examChooseInfoService;

    @Autowired
    @Qualifier("subjectInfoServiceImpl")
    private SubjectInfoService subjectInfoService;

    /**
     * 查询待考信息
     *
     * @return
     */
    @GetMapping("/willexams")
    public String getTestCenter(Integer classId, Integer adminId, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("classId", classId);
        map.put("adminId", adminId);

        List<ExamPlanInfo> examPlans = examPlanInfoService.getStudentWillExam(map);
//        List<ExamPlanInfo> examPlans = examPlanInfoService.getStudentWillExam(map);
//        model.addObject("examPlans", examPlans);
//        model.addObject("gradeId", gradeId);
        request.getSession().setAttribute("examPlans", examPlans);
        request.getSession().setAttribute("classId", classId);

        return "reception/examCenter";
    }

    @RequestMapping("/history")
    public String getTestHistory(Integer adminId, HttpServletRequest request) {
        List<ExamHistoryPaper> examHistoryToStudent = examHistoryInfoService.getExamHistoryToStudent(adminId);
        request.getSession().setAttribute("examHistoryToStudent", examHistoryToStudent);
        return "reception/examHistory";
    }


    @GetMapping("/review")
    private  String review(){
        return "reception/review";
    }
    @GetMapping("/begin")
    private String beginTest(Integer examPaperId,Integer adminId, HttpServletRequest request) {
        ExamPaperInfo examPaperById = examPaperInfoService.getExamPaperById(examPaperId);
        ArrayList<SubjectInfo> radioSub = new ArrayList<>();
        ArrayList<SubjectInfo> shortSub = new ArrayList<>();
        List<SubjectInfo> examPaperWithPaperId = subjectMiddleInfoService.getExamPaperWithPaperId(examPaperById.getExamPaperId());
        Admin adminById = adminService.getAdminById(adminId);
        for (SubjectInfo sub:examPaperWithPaperId
        ) {
            if (sub.getDivision()==1)
                radioSub.add(sub);
            if (sub.getDivision()==2)
                shortSub.add(sub);
        }
        int examPaperTime = examPaperById.getExamPaperTime()*60;
        int minute = parseInt(String.valueOf(((examPaperTime / 60) % 60)));
        int hour = parseInt(String.valueOf(((examPaperTime / 60) / 60)));
        String min = minute < 10 ? "0" + String.valueOf(minute) : String.valueOf(minute);
        String hou = hour < 10 ? "0" + String.valueOf(hour) : String.valueOf(hour);
        request.getSession().setAttribute("examPaperById",examPaperById);
        request.getSession().setAttribute("radioSub",radioSub);
        request.getSession().setAttribute("shortSub",shortSub);
        request.getSession().setAttribute("adminById",adminById);
        request.setAttribute("times",hou+":"+min);
        return "reception/begin";
    }

    @GetMapping("/choose")
    private void chooseAndAnswer(
                                   Integer adminId,
                                   Integer examPaperId,
                                   Integer subjectId,
                                   String chooseResult,
                                   HttpServletResponse response)throws IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("adminId", adminId);
        map.put("examPaperId", examPaperId);
        map.put("subjectId", subjectId);
        ExamChooseInfo examChoose = examChooseInfoService.getChooseWithIds(map);
        if (examChoose == null) {
            map.put("chooseResult", chooseResult);
            /** 添加选择记录 */
            examChooseInfoService.addChoose(map);
        }
        else if (examChoose.getChooseId() != null && examChoose != null) {
            /*
             * 如果选择了和上次相同的答案，则不做修改操作
             * 优化 -- 前台判断选择了相同答案则不发出请求
             */
            if(!chooseResult.equals(examChoose.getChooseResult())) {
                examChoose.setChooseResult(chooseResult);
                /** 当前选择答案和之前选择答案不同 修改答案记录 */
                examChooseInfoService.updateChooseWithIds(examChoose);
            }
        } else {
            response.getWriter().print("f");
            return;
        }
        response.getWriter().print(map);
    }

    @GetMapping("/test_jiaojuan/{adminId}/{examPaperId}")
    private String jiaoJuan(@PathVariable("adminId") Integer adminId,@PathVariable("examPaperId") Integer examPaperId, HttpServletRequest request){

        Float radioScore = 0.0f;
        Float shortScore = 0.0f;
        SimilarityUtils similarityUtils = new SimilarityUtils();

        Map<String, Object> map = new HashMap<String, Object>();
        ArrayList<SubjectInfo> radioSub = new ArrayList<>();
        ArrayList<SubjectInfo> shortSub = new ArrayList<>();
        map.put("adminId", adminId);
        map.put("examPaperId", examPaperId);
        List<ExamChooseInfo> chooseInfoWithExamSubject = examChooseInfoService.getChooseInfoWithExamSubject(map);
        for (ExamChooseInfo examChooseInfo: chooseInfoWithExamSubject
             ) {
            SubjectInfo subjectWithId = subjectInfoService.getSubjectWithId(examChooseInfo.getSubject().getSubjectId());
            if (subjectWithId.getDivision() == 1) {
                if (subjectWithId.getRightResult().equals(examChooseInfo.getChooseResult())) {
                    radioSub.add(subjectWithId);
                    radioScore = radioScore + subjectWithId.getSubjectScore();
                }
            }
            if (subjectWithId.getDivision() == 2) {
                Float levenshtein = similarityUtils.levenshtein(subjectWithId.getRightResult(), examChooseInfo.getChooseResult());
                if (levenshtein > 0.05) {
                    shortSub.add(subjectWithId);
                    shortScore = shortScore + subjectWithId.getSubjectScore() * levenshtein;
                }
            }
        }

//        添加历史记录
        Map<String,Object> map1 = new HashMap<>();
        map1.put("admintId",adminId);
        map1.put("examPaperId",examPaperId);
        map1.put("examScore",Math.round(shortScore+radioScore));
        int addExamHistory = examHistoryInfoService.isAddExamHistory(map1);
//        删除考试信息

        Map<String, Object> map3 = new HashMap<String, Object>();

        Admin adminById = adminService.getAdminById(adminId);
        map.put("classId", adminById.getClassInfo().getClassId());
        map.put("adminId", adminId);
        List<ExamPlanInfo> examPlans = examPlanInfoService.getStudentWillExam(map3);
        for (ExamPlanInfo e: examPlans
             ) {
            examPlanInfoService.isDeleExamPlan(e.getExamPlanId());
        }

        request.setAttribute("radioRight",radioSub.size());
        request.setAttribute("shortRight",shortSub.size());
        request.setAttribute("radioScore",Math.round(radioScore));
        request.setAttribute("shortScore",Math.round(shortScore));
        request.setAttribute("allRight",radioSub.size()+shortSub.size());
        request.setAttribute("score",Math.round(shortScore+radioScore));

        return "reception/test_jiaojuan";
    }
}
