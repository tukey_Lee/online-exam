package com.tukey.controller;

import com.tukey.pojo.*;
import com.tukey.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class TeaExamController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;

    @Autowired
    @Qualifier("classInfoServiceImpl")
    private ClassInfoService classInfoService;

    @Autowired
    @Qualifier("courseInfoServiceImpl")
    private CourseInfoService courseInfoService;

    @Autowired
    @Qualifier("examPaperInfoServiceImpl")
    private ExamPaperInfoService examPaperInfoService;

    @Autowired
    @Qualifier("subjectInfoServiceImpl")
    private SubjectInfoService subjectInfoService;

    @Autowired
    @Qualifier("examPlanInfoServiceImpl")
    private ExamPlanInfoService examPlanInfoService;

    @Autowired
    @Qualifier("examHistoryInfoServiceImpl")
    private ExamHistoryInfoService examHistoryInfoService;


    @Autowired
    @Qualifier("examSubjectMiddleInfoServiceImpl")
    private ExamSubjectMiddleInfoService subjectMiddleInfoService;


    @Autowired
    @Qualifier("examChooseInfoServiceImpl")
    private ExamChooseInfoService examChooseInfoService;



    @GetMapping("/papers")
    public String papers(HttpServletRequest request) {
        List<ExamPaperInfo> examPapersClear = examPaperInfoService.getExamPapersClear();
        request.getSession().setAttribute("examPapersClear", examPapersClear);
        return "admin/reception/papers";
    }
    @GetMapping("/addSubForPap")
    public String addSubForPap(Integer examPaperId,HttpServletRequest request){
        List<SubjectInfo> allSubjects = subjectInfoService.getAllSubjects();
        List<CourseInfo> courses = courseInfoService.getCourses();
        request.getSession().setAttribute("allSubjects",allSubjects);
        request.setAttribute("courses",courses);
        request.setAttribute("examPaperId",examPaperId);
        return "admin/reception/addSubForPap";
    }
    @GetMapping("/zujuan")
    public void zujuan(String subjectId,Integer examPaperId,HttpServletResponse response) throws IOException{
        //添加试题总分统计
        int scoreSum = 0;
        //添加试题总量统计
        int subjectSum = 0;
        HashMap<String,Object> map = new HashMap<>();
        map.put("examPaperId",examPaperId);
        ArrayList<Integer> subjectIds = new ArrayList<Integer>();

        String[] subIds = subjectId.split("-");
        for (String s:subIds
             ) {
            subjectIds.add(Integer.parseInt(s));
            SubjectInfo subjectWithId = subjectInfoService.getSubjectWithId(Integer.parseInt(s));
            //累加试题分数
            scoreSum += subjectWithId.getSubjectScore();
            //累加试题数量
            subjectSum += 1;
        }
        /** 需要添加试题集合 */
        map.put("subjectIds", subjectIds);

        Map<String, Object> scoreWithNum = new HashMap<String, Object>();
        scoreWithNum.put("subjectNum", subjectSum);
        scoreWithNum.put("score", scoreSum);
        scoreWithNum.put("examPaperId", examPaperId);
        /** 修改试卷总分 */
        examPaperInfoService.isUpdateExamPaperScore(scoreWithNum);
        /** 修改试卷试题总量 */
        examPaperInfoService.isUpdateExamPaperSubjects(scoreWithNum);
        /** 添加试题到试卷中 */
        subjectMiddleInfoService.isAddESM(map);
        response.getWriter().print("t");
    }
    @GetMapping("/seeSubForPap")
    public String seeSubForPap(Integer examPaperId,HttpServletRequest request){
        ExamPaperInfo examPaperById = examPaperInfoService.getExamPaperById(examPaperId);
        ArrayList<SubjectInfo> radioSub = new ArrayList<>();
        ArrayList<SubjectInfo> shortSub = new ArrayList<>();
        List<SubjectInfo> examPaperWithPaperId = subjectMiddleInfoService.getExamPaperWithPaperId(examPaperById.getExamPaperId());
        for (SubjectInfo sub:examPaperWithPaperId
        ) {
            if (sub.getDivision()==1)
                radioSub.add(sub);
            if (sub.getDivision()==2)
                shortSub.add(sub);
        }
        request.getSession().setAttribute("examPaperById",examPaperById);
        request.getSession().setAttribute("radioSub",radioSub);
        request.getSession().setAttribute("shortSub",shortSub);
        return "admin/reception/seeSubForPap";
    }

    @GetMapping("/questions")
    public String questions(HttpServletRequest request) {
        List<SubjectInfo> allSubjects = subjectInfoService.getAllSubjects();
        List<CourseInfo> courses = courseInfoService.getCourses();
        request.setAttribute("courses",courses);
        request.getSession().setAttribute("allSubjects",allSubjects);
        return "admin/reception/questions";
    }

    @GetMapping("/timeTable")
    public String timeTable(HttpServletRequest request) {
        List<ExamPlanInfo> allExamPlans1 = (List<ExamPlanInfo>) request.getSession().getAttribute("allExamPlans");
        List<ExamPlanInfo> allExamPlans = examPlanInfoService.getAllExamPlans();
        if (allExamPlans1==null){
            request.getSession().setAttribute("allExamPlans",allExamPlans);
        }else {
            request.getSession().setAttribute("allExamPlans",allExamPlans1);
        }

        return "admin/reception/timeTable";
    }
    @GetMapping("/addTimeTable")
    public String addTimeTable(HttpServletRequest request){

        List<ExamPlanInfo> allExamPlans = examPlanInfoService.getAllExamPlans();
        List<ExamPaperInfo> examPapersClear = examPaperInfoService.getExamPapersClear();
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        List<CourseInfo> courses = courseInfoService.getCourses();
        request.setAttribute("allExamPlans",allExamPlans);
        request.setAttribute("examPapersClear",examPapersClear);
        request.setAttribute("allClasses",allClasses);
        request.setAttribute("courses",courses);
        return "admin/reception/addTimeTable";
    }
    @GetMapping("/subNum")
    public void subNum(HttpServletResponse response,Integer examPaperId) throws IOException{
        List<SubjectInfo> examPaperWithPaperId = subjectMiddleInfoService.getExamPaperWithPaperId(examPaperId);
        response.getWriter().print(examPaperWithPaperId.size());
    }
    @PostMapping("/addTimeTable")
    public void addTimeTable_btn(
            ExamPlanInfo examPlanInfo,
            Integer examPaperId,
            Integer classId,
            Integer courseId,
            HttpServletRequest request,
            HttpServletResponse response) throws IOException, ParseException {
        String msg = "success";

        Map<String,Object> hashMap = new HashMap<>();

        Date time = new SimpleDateFormat("yyyy-MM-dd").parse(examPlanInfo.getBeginTime());
        String nowTime = new SimpleDateFormat("yyyy-MM-dd").format(time);

        CourseInfo courseById = courseInfoService.getCourseById(courseId);
        ClassInfo classById = classInfoService.getClassById(classId);
        ExamPaperInfo examPaperById = examPaperInfoService.getExamPaperById(examPaperId);
        examPlanInfo.setCourse(courseById);
        examPlanInfo.setClassInfo(classById);
        examPlanInfo.setExamPaper(examPaperById);
        examPlanInfo.setBeginTime(nowTime);

//        int addExamPlan = examPlanInfoService.isAddExamPlan(examPlanInfo);
        List<ExamPlanInfo>  allExamPlans = (List<ExamPlanInfo>)  request.getSession().getAttribute("allExamPlans");
        allExamPlans.add(examPlanInfo);

        request.getSession().setAttribute("allExamPlans",allExamPlans);

        response.getWriter().print(msg);

    }
    @GetMapping("/deleTimeTable")
    public void deleTimeTable(Integer examPlanId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String msg = "success";
        int removeExamPlan = examPlanInfoService.isRemoveExamPlan(examPlanId);
        if (removeExamPlan <= 0) {
            msg = "error";
        }
        List<ExamPlanInfo> allExamPlans = (List<ExamPlanInfo>) request.getSession().getAttribute("allExamPlans");
        allExamPlans.removeIf(e -> e.getExamPlanId().equals(examPlanId));
        request.getSession().setAttribute("allExamPlans",allExamPlans);
        request.setAttribute("msg", msg);
        response.sendRedirect("http://localhost:8848/timeTable");
    }

//======================================================================

    @GetMapping("/addPaper")
    public String addPaper(HttpServletRequest request){
        List<ExamPaperInfo> examPapersClear = examPaperInfoService.getExamPapersClear();
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.getSession().setAttribute("examPapersClear",examPapersClear);
        request.setAttribute("allClasses",allClasses);
        return "admin/reception/addPaper";
    }
    @PostMapping("/addPaper")
    public void addPaper_btn(ExamPaperInfo examPaperInfo,String className,HttpServletResponse response) throws IOException{
        String msg = "success";
        ClassInfo classByName = classInfoService.getClassByName(className);
        examPaperInfo.setClassInfo(classByName);
        int addExamPaper = examPaperInfoService.isAddExamPaper(examPaperInfo);
        response.getWriter().print(msg);
    }
    @GetMapping("/editPaper")
    public String editPaper(Integer examPaperId,HttpServletRequest request){
        ExamPaperInfo examPaperById = examPaperInfoService.getExamPaperById(examPaperId);
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        request.setAttribute("examPaperById",examPaperById);
        request.setAttribute("allClasses",allClasses);
        return "admin/reception/editPaper";
    }
    @PostMapping("/editPaper")
    public void editPaper_btn(ExamPaperInfo examPaperInfo,String className,HttpServletResponse response) throws IOException{
        System.out.println(examPaperInfo);
        String msg = "error";
        ClassInfo classByName = classInfoService.getClassByName(className);
        examPaperInfo.setClassInfo(classByName);
        int updateExamPaper = examPaperInfoService.isUpdateExamPaper(examPaperInfo);
        if (updateExamPaper > 0) {
            msg = "success";
            response.getWriter().print(msg);
        } else {
            response.getWriter().print(msg);
        }
    }
    @GetMapping("/delPaper")
    public void delPaper(Integer examPaperId,HttpServletRequest request, HttpServletResponse response) throws IOException{
        String msg = "success";
        int delExamPaper = examPaperInfoService.isDelExamPaper(examPaperId);
        if (delExamPaper<=0){
            msg = "error";
        }
        request.setAttribute("msg", msg);
        response.sendRedirect("http://localhost:8848/papers");
    }

    @GetMapping("/addQuestion")
    public String addQuestion(HttpServletRequest request){
        int mulSub = -1;
        int shoSub = -1;
        List<SubjectInfo> allSubjects = subjectInfoService.getAllSubjects();
        List<CourseInfo> courses = courseInfoService.getCourses();
        for (SubjectInfo sub: allSubjects
             ) {
            if (sub.getDivision()==1){
                mulSub = sub.getSubjectId();
            }
            if (sub.getDivision()==2){
                shoSub = sub.getSubjectId();
            }
        }
        if (mulSub!=-1&&shoSub!=-1){
            request.setAttribute("mulSubId",mulSub);
            request.setAttribute("shoSubId",shoSub);
        }
        request.setAttribute("courses",courses);
        return "admin/reception/addQuestion";
    }
    public String trim(String source, String beTrim) {
        if(source==null){
            return "";
        }
        source = source.trim(); // 循环去掉字符串首的beTrim字符
        if(source.isEmpty()){
            return "";
        }
        String beginChar = source.substring(0, 1);
        if (beginChar.equalsIgnoreCase(beTrim)) {
            source = source.substring(1, source.length());
            beginChar = source.substring(0, 1);
        }
        // 循环去掉字符串尾的beTrim字符
        String endChar = source.substring(source.length() - 1, source.length());
        if (endChar.equalsIgnoreCase(beTrim)) {
            source = source.substring(0, source.length() - 1);
            endChar = source.substring(source.length() - 1, source.length());
        }
        return source;
    }
    @PostMapping("/addQuestion")
    public void addQuestion_btn(
            Integer subjectId,
            String subjectName,
            String optionA,
            String optionB,
            String optionC,
            String optionD,
            String rightResult,
            Integer subjectScore,
            String courseName,
            Integer division,
            HttpServletRequest request,
            HttpServletResponse response ) throws  IOException{
        String msg = "success";
//        int addSubject = subjectInfoService.isAddSubject(
//                new SubjectInfo(subjectId,trim(subjectName,","),
//                        trim(optionA,","),trim(optionB,","),
//                        trim(optionC,","),trim(optionD,","),
//                        trim(rightResult,","),subjectScore,
//                        null,0,
//                        division,null,
//                        null));
        SubjectInfo subjectInfo = new SubjectInfo(subjectId, trim(subjectName, ","),
                trim(optionA, ","), trim(optionB, ","),
                trim(optionC, ","), trim(optionD, ","),
                trim(rightResult, ","), subjectScore,
                null, 0,
                division, null,
                null);


        response.getWriter().print(msg);
    }
    @GetMapping("/editQuestion")
    public String editQuestion(HttpServletRequest request,Integer subjectId){
        SubjectInfo subjectWithId = subjectInfoService.getSubjectWithId(subjectId);
        List<CourseInfo> courses = courseInfoService.getCourses();

        request.getSession().setAttribute("subjectWithId",subjectWithId);
        request.setAttribute("courses",courses);
        return "admin/reception/editQuestion";
    }
    @PostMapping("/editQuestion")
    public void editQuestion_btn(HttpServletResponse response) throws IOException{
        String msg = "success";
        response.getWriter().print(msg);
    }
    @GetMapping("/delQuestion")
    public void delQuestion(Integer subjectId,HttpServletRequest request, HttpServletResponse response) throws IOException{
        String msg = "auccess";
        int delSubject = subjectInfoService.isDelSubject(subjectId);
        request.setAttribute("msg",msg);
        response.sendRedirect("http://localhost:8848/questions");
    }
    @GetMapping("/editTimeTable")
    public String editTimeTable(HttpServletRequest request,Integer examPlanId){

        ExamPlanInfo examPlanById = examPlanInfoService.getExamPlanById(examPlanId);
        List<ClassInfo> allClasses = classInfoService.getAllClasses();
        List<CourseInfo> courses = courseInfoService.getCourses();
        List<ExamPaperInfo> examPapersClear = examPaperInfoService.getExamPapersClear();
        request.setAttribute("allClasses",allClasses);

        request.setAttribute("courses",courses);
        request.setAttribute("examPapersClear",examPapersClear);
        request.setAttribute("examPlanByIdWithTimeTable",examPlanById);
        request.setAttribute("examPlanId",examPlanId);
        return "admin/reception/editTimeTable";
    }
    @PostMapping("/editTimeTable")
    public void editTimeTable_btn(
            ExamPlanInfo examPlanInfo,
            Integer examPaperId,
            Integer classId,
            Integer courseId,
            HttpServletRequest request,HttpServletResponse response) throws IOException, ParseException {
        String msg = "error";
        Date time = new SimpleDateFormat("yyyy-MM-dd").parse(examPlanInfo.getBeginTime());
        String nowTime = new SimpleDateFormat("yyyy-MM-dd").format(time);
        CourseInfo courseById = courseInfoService.getCourseById(courseId);
        ClassInfo classById = classInfoService.getClassById(classId);
        ExamPaperInfo examPaperById = examPaperInfoService.getExamPaperById(examPaperId);
        examPlanInfo.setCourse(courseById);
        examPlanInfo.setClassInfo(classById);
        examPlanInfo.setExamPaper(examPaperById);
        examPlanInfo.setBeginTime(nowTime);
        List<ExamPlanInfo> allExamPlans = (List<ExamPlanInfo>) request.getSession().getAttribute("allExamPlans");

        for (ExamPlanInfo e:allExamPlans
             ) {
            if (e.getExamPlanId().equals(examPlanInfo.getExamPlanId())){
                allExamPlans.remove(e);
                allExamPlans.add(examPlanInfo);
                msg = "success";
            }
        }
        request.getSession().setAttribute("allExamPlans",allExamPlans);

        response.getWriter().print(msg);
    }
//==============================================================

    @GetMapping("/historyTea")
    public String historyTea(HttpServletRequest request) {
        List<ExamHistoryInfo> examHistoryToTeacher = examHistoryInfoService.getExamHistoryToTeacher();
        request.getSession().setAttribute("examHistoryToTeacher",examHistoryToTeacher);
        return "admin/reception/historyTea";
    }
    @GetMapping("/eyeHistory")
    public String eyeHistory(Integer historyId, Integer adminId,HttpServletRequest request){
        ExamHistoryInfo historyInfoWithId = examHistoryInfoService.getAllHistoryInfoWithId(historyId);
        Admin adminById = adminService.getAdminById(adminId);
        historyInfoWithId.setAdmin(adminById);
        List<SubjectInfo> examPaperWithPaperId = subjectMiddleInfoService.getExamPaperWithPaperId(historyInfoWithId.getExamPaper().getExamPaperId());
        ArrayList<SubjectInfo> radioSub = new ArrayList<>();
        ArrayList<SubjectInfo> shortSub = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        map.put("adminId",adminId);
        map.put("examPaperId",historyInfoWithId.getExamPaper().getExamPaperId());
        List<ExamChooseInfo> getChooseInfoWithSumScore = examChooseInfoService.getChooseInfoWithSumScore(map);

        for (SubjectInfo sub:examPaperWithPaperId
             ) {
            if (sub.getDivision()==1)
                radioSub.add(sub);
            if (sub.getDivision()==2)
                shortSub.add(sub);
        }

        request.getSession().setAttribute("historyInfoWithId",historyInfoWithId);
        request.getSession().setAttribute("radioSub",radioSub);
        request.getSession().setAttribute("shortSub",shortSub);
        request.getSession().setAttribute("getChooseInfoWithSumScore",getChooseInfoWithSumScore);
        return "eyeHistory";
    }
}
