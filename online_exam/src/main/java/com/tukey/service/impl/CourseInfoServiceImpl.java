package com.tukey.service.impl;

import com.tukey.dao.CourseInfoMapper;
import com.tukey.pojo.CourseInfo;
import com.tukey.service.CourseInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseInfoServiceImpl implements CourseInfoService {


    private CourseInfoMapper courseInfoMapper;

    public void setCourseInfoMapper(CourseInfoMapper courseInfoMapper) {
        this.courseInfoMapper = courseInfoMapper;
    }

    @Override
    public List<CourseInfo> getCourses() {
        return courseInfoMapper.getCourses();
    }

    @Override
    public int isUpdateCourse(CourseInfo course) {
        return courseInfoMapper.isUpdateCourse(course);
    }

    @Override
    public int isAddCourse(CourseInfo course) {
        return courseInfoMapper.isAddCourse(course);
    }

    @Override
    public int isDelCourse(int courseId) {
        return courseInfoMapper.isDelCourse(courseId);
    }

    @Override
    public CourseInfo getCourseById(int courseId) {
        return courseInfoMapper.getCourseById(courseId);
    }

    @Override
    public CourseInfo getCourseByName(String courseName) {
        return courseInfoMapper.getCourseByName(courseName);
    }


}
