package com.tukey.service.impl;

import com.tukey.dao.ExamPaperInfoMapper;
import com.tukey.pojo.ExamPaperInfo;
import com.tukey.service.ExamPaperInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExamPaperInfoServiceImpl implements ExamPaperInfoService {

    private ExamPaperInfoMapper examPaperInfoMapper;

    public void setExamPaperInfoMapper(ExamPaperInfoMapper examPaperInfoMapper) {
        this.examPaperInfoMapper = examPaperInfoMapper;
    }


    @Override
    public List<ExamPaperInfo> getExamPapers(Map<String, Object> map) {
        return examPaperInfoMapper.getExamPapers(map);
    }

    @Override
    public ExamPaperInfo getExamPaperById(int examPaperId) {
        return examPaperInfoMapper.getExamPaperById(examPaperId);
    }

    @Override
    public ExamPaperInfo getExamPaperByName(String examPaperName) {
        return examPaperInfoMapper.getExamPaperByName(examPaperName);
    }

    @Override
    public List<ExamPaperInfo> getExamPapersClear() {
        return examPaperInfoMapper.getExamPapersClear();
    }

    @Override
    public int isAddExamPaper(ExamPaperInfo examPaper) {
        return examPaperInfoMapper.isAddExamPaper(examPaper);
    }

    @Override
    public int isUpdateExamPaper(ExamPaperInfo examPaper) {
        return examPaperInfoMapper.isUpdateExamPaper(examPaper);
    }

    @Override
    public int isDelExamPaper(int examPaperId) {
        return examPaperInfoMapper.isDelExamPaper(examPaperId);
    }

    @Override
    public int getExamPaperTotal() {
        return examPaperInfoMapper.getExamPaperTotal();
    }

    @Override
    public int isUpdateExamPaperSubjects(Map<String, Object> map) {
        return examPaperInfoMapper.isUpdateExamPaperSubjects(map);
    }

    @Override
    public int isUpdateExamPaperScore(Map<String, Object> map) {
        return examPaperInfoMapper.isUpdateExamPaperScore(map);
    }
}
