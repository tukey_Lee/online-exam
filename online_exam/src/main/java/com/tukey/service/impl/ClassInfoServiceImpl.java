package com.tukey.service.impl;

import com.tukey.dao.ClassInfoMapper;
import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import com.tukey.service.ClassInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ClassInfoServiceImpl implements ClassInfoService {

    private ClassInfoMapper classInfoMapper;

    public void setClassInfoMapper(ClassInfoMapper classInfoMapper) {
        this.classInfoMapper = classInfoMapper;
    }

    @Override
    public List<ClassInfo> getClasses(ClassInfo classInfo) {
        return classInfoMapper.getClasses(classInfo);
    }

    @Override
    public List<ClassInfo> getAllClasses() {
        return classInfoMapper.getAllClasses();
    }

    @Override
    public int isAddClass(ClassInfo classInfo) {
        return classInfoMapper.isAddClass(classInfo);
    }

    @Override
    public int isDelClass(int classId) {
        return classInfoMapper.isDelClass(classId);
    }

    @Override
    public ClassInfo getClassById(int classId) {
        return classInfoMapper.getClassById(classId);
    }

    @Override
    public ClassInfo getClassByName(String className) {
        return classInfoMapper.getClassByName(className);
    }

    @Override
    public ClassInfo getClassByTeacherId(int adminId) {
        return classInfoMapper.getClassByTeacherId(adminId);
    }

    @Override
    public int isUpdateClass(ClassInfo classInfo) {
        return classInfoMapper.isUpdateClass(classInfo);
    }

    @Override
    public Map<String, Object> getStudentCountForClass(Integer classId) {
        return classInfoMapper.getStudentCountForClass(classId);
    }
}
