package com.tukey.service.impl;

import com.tukey.dao.ExamPlanInfoMapper;
import com.tukey.pojo.ExamPlanInfo;
import com.tukey.service.ExamPlanInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExamPlanInfoServiceImpl implements ExamPlanInfoService {

    private ExamPlanInfoMapper examPlanInfoMapper;

    public void setExamPlanInfoMapper(ExamPlanInfoMapper examPlanInfoMapper) {
        this.examPlanInfoMapper = examPlanInfoMapper;
    }

    @Override
    public List<ExamPlanInfo> getExamPlans(Map<String, Object> map) {
        return examPlanInfoMapper.getExamPlans(map);
    }

    @Override
    public List<ExamPlanInfo> getAllExamPlans() {
        return examPlanInfoMapper.getAllExamPlans();
    }

    @Override
    public int isAddExamPlan(ExamPlanInfo examPlan) {
        return examPlanInfoMapper.isAddExamPlan(examPlan);
    }

    @Override
    public int isUpdateExamPlan(ExamPlanInfo examPlan) {
        return examPlanInfoMapper.isUpdateExamPlan(examPlan);
    }

    @Override
    public int isDeleExamPlan(int examPlanId) {
        return examPlanInfoMapper.isDeleExamPlan(examPlanId);
    }

    @Override
    public ExamPlanInfo getExamPlanById(int examPlanId) {
        return examPlanInfoMapper.getExamPlanById(examPlanId);
    }

    @Override
    public List<ExamPlanInfo> getStudentWillExam(Map<String, Object> map) {
        return examPlanInfoMapper.getStudentWillExam(map);
    }

    @Override
    public int isRemoveExamPlan(int examPlanId) {
        return examPlanInfoMapper.isRemoveExamPlan(examPlanId);
    }
}
