package com.tukey.service.impl;

import com.tukey.dao.ExamSubjectMiddleInfoMapper;
import com.tukey.pojo.ExamSubjectMiddleInfo;
import com.tukey.pojo.SubjectInfo;
import com.tukey.service.ExamSubjectMiddleInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExamSubjectMiddleInfoServiceImpl implements ExamSubjectMiddleInfoService {

    private ExamSubjectMiddleInfoMapper examSubjectMiddleInfoMapper;

    public void setExamSubjectMiddleInfoMapper(ExamSubjectMiddleInfoMapper examSubjectMiddleInfoMapper) {
    this.examSubjectMiddleInfoMapper = examSubjectMiddleInfoMapper;
    }

    @Override
    public List<ExamSubjectMiddleInfo> getExamPaperWithSubject(ExamSubjectMiddleInfo esm) {
        return examSubjectMiddleInfoMapper.getExamPaperWithSubject(esm);
    }

    @Override
    public List<SubjectInfo> getExamPaperWithPaperId(Integer examPaperId) {
        return examSubjectMiddleInfoMapper.getExamPaperWithPaperId(examPaperId);
    }

    @Override
    public int isAddESM(Map<String, Object> map) {
        return examSubjectMiddleInfoMapper.isAddESM(map);
    }

    @Override
    public int removeSubjectWithExamPaper(Map<String, Object> map) {
        return examSubjectMiddleInfoMapper.removeSubjectWithExamPaper(map);
    }

    @Override
    public Integer getEsmByExamIdWithSubjectId(ExamSubjectMiddleInfo esm) {
        return examSubjectMiddleInfoMapper.getEsmByExamIdWithSubjectId(esm);
    }
}
