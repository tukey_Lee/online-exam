package com.tukey.service.impl;


import com.tukey.dao.SubjectInfoMapper;
import com.tukey.pojo.SubjectInfo;
import com.tukey.service.SubjectInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SubjectInfoServiceImpl implements SubjectInfoService {

    private SubjectInfoMapper subjectInfoMapper;

    public void setSubjectInfoMapper(SubjectInfoMapper subjectInfoMapper) {
        this.subjectInfoMapper = subjectInfoMapper;
    }

    @Override
    public List<SubjectInfo> getSubjects(Map<String, Object> map) {
        return subjectInfoMapper.getSubjects(map);
    }

    @Override
    public List<SubjectInfo> getAllSubjects() {
        return subjectInfoMapper.getAllSubjects();
    }

    @Override
    public SubjectInfo getSubjectWithId(int subjectId) {
        return subjectInfoMapper.getSubjectWithId(subjectId);
    }

    @Override
    public int getSubjectTotal() {
        return subjectInfoMapper.getSubjectTotal();
    }

    @Override
    public int isAddSubject(SubjectInfo subject) {
        return subjectInfoMapper.isAddSubject(subject);
    }

    @Override
    public int isUpdateSubject(SubjectInfo subject) {
        return subjectInfoMapper.isUpdateSubject(subject);
    }

    @Override
    public int isDelSubject(int subjectId) {
        return subjectInfoMapper.isDelSubject(subjectId);
    }

    @Override
    public int isAddSubjects(Map<String, Object> map) {
        return subjectInfoMapper.isAddSubjects(map);
    }
}
