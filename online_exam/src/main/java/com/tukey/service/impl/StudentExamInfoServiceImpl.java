package com.tukey.service.impl;

import com.tukey.dao.StudentExamInfoMapper;
import com.tukey.pojo.StudentExamInfo;
import com.tukey.service.StudentExamInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentExamInfoServiceImpl implements StudentExamInfoService {

    private StudentExamInfoMapper studentExamInfoMapper;

    public void setStudentExamInfoMapper(StudentExamInfoMapper studentExamInfoMapper) {
    this.studentExamInfoMapper = studentExamInfoMapper;
    }

    @Override
    public List<StudentExamInfo> getStudentExamCountByClassId(int classId) {
        return studentExamInfoMapper.getStudentExamCountByClassId(classId);
    }

    @Override
    public List<StudentExamInfo> getStudentExamInfo(int studentId) {
        return studentExamInfoMapper.getStudentExamInfo(studentId);
    }

    @Override
    public List<StudentExamInfo> getAllStudentAvgScoreCount(int classId) {
        return studentExamInfoMapper.getAllStudentAvgScoreCount(classId);
    }
}
