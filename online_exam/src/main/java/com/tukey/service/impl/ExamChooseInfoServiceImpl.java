package com.tukey.service.impl;

import com.tukey.dao.ExamChooseInfoMapper;
import com.tukey.pojo.ExamChooseInfo;
import com.tukey.service.ExamChooseInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExamChooseInfoServiceImpl implements ExamChooseInfoService {

    private ExamChooseInfoMapper examChooseInfoMapper;

    public void setExamChooseInfoMapper(ExamChooseInfoMapper examChooseInfoMapper) {
        this.examChooseInfoMapper = examChooseInfoMapper;
    }

    @Override
    public ExamChooseInfo getChooseWithIds(Map<String, Object> map) {
        return examChooseInfoMapper.getChooseWithIds(map);
    }

    @Override
    public int updateChooseWithIds(ExamChooseInfo examChoose) {
        return examChooseInfoMapper.updateChooseWithIds(examChoose);
    }

    @Override
    public int addChoose(Map<String, Object> map) {
        return examChooseInfoMapper.addChoose(map);
    }

    @Override
    public List<ExamChooseInfo> getChooseInfoWithSumScore(Map<String, Object> map) {
        return examChooseInfoMapper.getChooseInfoWithSumScore(map);
    }

    @Override
    public List<ExamChooseInfo> getChooseInfoWithExamSubject(Map<String, Object> map) {
        return examChooseInfoMapper.getChooseInfoWithExamSubject(map);
    }
}
