package com.tukey.service.impl;

import com.tukey.dao.ExamHistoryInfoMapper;
import com.tukey.pojo.ExamHistoryInfo;
import com.tukey.pojo.ExamHistoryPaper;
import com.tukey.service.ExamHistoryInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExamHistoryInfoServiceImpl implements ExamHistoryInfoService {

    private ExamHistoryInfoMapper examHistoryInfoMapper;

    public void setExamHistoryInfoMapper(ExamHistoryInfoMapper examHistoryInfoMapper) {
        this.examHistoryInfoMapper = examHistoryInfoMapper;
    }

    @Override
    public List<ExamHistoryPaper> getExamHistoryToStudent(int adminId) {
        return examHistoryInfoMapper.getExamHistoryToStudent(adminId);
    }

    @Override
    public int isAddExamHistory(Map<String, Object> map) {
        return examHistoryInfoMapper.isAddExamHistory(map);
    }

    @Override
    public int getHistoryInfoWithIds(Map<String, Object> map) {
        return examHistoryInfoMapper.getHistoryInfoWithIds(map);
    }

    @Override
    public ExamHistoryInfo getAllHistoryInfoWithId(Integer historyId) {
        return examHistoryInfoMapper.getAllHistoryInfoWithId(historyId);
    }

    @Override
    public List<ExamHistoryInfo> getExamHistoryToTeacher() {
        return examHistoryInfoMapper.getExamHistoryToTeacher();
    }
}
