package com.tukey.service.impl;

import com.tukey.dao.AdminMapper;
import com.tukey.pojo.Admin;
import com.tukey.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class AdminServiceImpl implements AdminService {

    private AdminMapper adminMapper;

    public void setAdminMapper(AdminMapper adminMapper) {
        this.adminMapper = adminMapper;
    }


    @Override
    public List<Admin> getAdmins() {
        return adminMapper.getAdmins();
    }

    @Override
    public Admin getAdminById(int adminId) {
        return adminMapper.getAdminById(adminId);
    }

    @Override
    public int isUpdateAdmin(Admin admin) {
        return adminMapper.isUpdateAdmin(admin);
    }

    @Override
    public int isDelAdmin(int adminId) {
        return adminMapper.isDelAdmin(adminId);
    }

    @Override
    public int isAddAdmin(Admin admin) {
        return adminMapper.isAddAdmin(admin);
    }

    @Override
    public int getStuTotal(int classId) {
        return adminMapper.getStuTotal(classId);
    }

    @Override
    public Admin getTeaTotal(int classId) {
        return adminMapper.getTeaTotal(classId);
    }

    @Override
    public Admin getAdminByAccountAndPwd(String account) {
        return adminMapper.getAdminByAccountAndPwd(account);
    }

    @Override
    public int isResetPwdWithAdmin(Admin admin) {
        return adminMapper.isResetPwdWithAdmin(admin);
    }

    @Override
    public List<Admin> getStudentsByClassId(int classId) {
        return adminMapper.getStudentsByClassId(classId);
    }

    @Override
    public List<Admin> getAllTeachers() {
        return adminMapper.getAllTeachers();
    }
}
