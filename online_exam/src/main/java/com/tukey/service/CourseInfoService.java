package com.tukey.service;


import com.tukey.pojo.CourseInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseInfoService {

    public List<CourseInfo> getCourses();

    public int isUpdateCourse(CourseInfo course);

    public int isAddCourse(CourseInfo course);

    public int isDelCourse(int courseId);

    public CourseInfo getCourseById(int courseId);
    public CourseInfo getCourseByName(String courseName);

}
