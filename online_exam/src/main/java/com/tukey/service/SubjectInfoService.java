package com.tukey.service;


import com.tukey.pojo.SubjectInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SubjectInfoService {

    public List<SubjectInfo> getSubjects(Map<String, Object> map);

    public List<SubjectInfo> getAllSubjects();

    public SubjectInfo getSubjectWithId(int subjectId);

    public int getSubjectTotal();

    public int isAddSubject(SubjectInfo subject);

    public int isUpdateSubject(SubjectInfo subject);

    public int isDelSubject(int subjectId);

    //批量添加试题
    public int isAddSubjects(Map<String, Object> map);
}
