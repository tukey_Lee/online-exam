package com.tukey.service;


import com.tukey.pojo.StudentExamInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentExamInfoService {

    //后台教师根据查看某一班级下所有学生考试数量
    public List<StudentExamInfo> getStudentExamCountByClassId(int classId);

    //后台教师查看某一学生的考试信息
    public List<StudentExamInfo> getStudentExamInfo(int studentId);

    public List<StudentExamInfo> getAllStudentAvgScoreCount(int classId);
}
