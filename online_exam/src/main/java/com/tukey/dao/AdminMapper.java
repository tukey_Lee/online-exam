package com.tukey.dao;

import com.tukey.pojo.Admin;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 针对用户的所有操作
 */
@Repository
public interface AdminMapper {

    public List<Admin> getAdmins();

    public Admin getAdminById(int adminId);

    public int isUpdateAdmin(Admin admin);

    public int isDelAdmin(int adminId);

    public int isAddAdmin(Admin admin);

    public int getStuTotal(int classId);
    public Admin getTeaTotal(int classId);

    public Admin getAdminByAccountAndPwd(String account);

    public int isResetPwdWithAdmin(Admin admin);

    public List<Admin> getStudentsByClassId(int classId);

    public List<Admin> getAllTeachers();

}
