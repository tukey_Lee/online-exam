package com.tukey.dao;

import com.tukey.pojo.ExamHistoryInfo;
import com.tukey.pojo.ExamHistoryPaper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ExamHistoryInfoMapper {
    //查询考试历史信息，针对前台学生查询
    public List<ExamHistoryPaper> getExamHistoryToStudent(int adminId);

    public int isAddExamHistory(Map<String, Object> map);

    public int getHistoryInfoWithIds(Map<String, Object> map);

    public ExamHistoryInfo getAllHistoryInfoWithId(Integer historyId);

    //查询考试历史信息，针对后台教师查询
    public List<ExamHistoryInfo> getExamHistoryToTeacher();
}
