package com.tukey.dao;


import com.tukey.pojo.ExamSubjectMiddleInfo;
import com.tukey.pojo.SubjectInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ExamSubjectMiddleInfoMapper {
    public List<ExamSubjectMiddleInfo> getExamPaperWithSubject(ExamSubjectMiddleInfo esm);

    public List<SubjectInfo> getExamPaperWithPaperId(Integer examPaperId);

    public int isAddESM(Map<String, Object> map);

    public int removeSubjectWithExamPaper(Map<String, Object> map);

    public Integer getEsmByExamIdWithSubjectId(ExamSubjectMiddleInfo esm);
}
