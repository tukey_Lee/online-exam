package com.tukey.util;

import com.tukey.pojo.Admin;
import com.tukey.pojo.ClassInfo;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: zjk
 * @Date: 2019/8/30
 * @Description:
 */
public class AdminInfoImportUtil {

    public ArrayList<Object> getAdminInfo(String file) {

        ArrayList<Object> arrayList = new ArrayList<>();
        try {
            //创建工作簿
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(new FileInputStream(file));
//            System.out.println("xssfWorkbook对象：" + xssfWorkbook);
            //读取第一个工作表
            XSSFSheet sheet = xssfWorkbook.getSheetAt(0);
//            System.out.println("sheet对象：" + sheet);
            //获取最后一行的num，即总行数。此处从0开始计数
            int maxRow = sheet.getLastRowNum();
//            System.out.println("总行数为：" + maxRow);
            for (int row = 1; row <= maxRow; row++) {
                //获取最后单元格num，即总单元格数 ***注意：此处从1开始计数***
                int maxRol = sheet.getRow(row).getLastCellNum();
//                System.out.println("--------第" + row + "行的数据如下--------");
                Admin admin = new Admin();
                ClassInfo classInfo = new ClassInfo();
                for (int rol = 0; rol < maxRol; rol++) {
                    switch (rol){
                        case 0: admin.setAccount(sheet.getRow(row).getCell(rol).toString()); break;
                        case 1: admin.setName(sheet.getRow(row).getCell(rol).toString()); break;
                        case 2: admin.setPassword(sheet.getRow(row).getCell(rol).toString()); break;
                        case 3: classInfo.setClassName(sheet.getRow(row).getCell(rol).toString()); break;
                    }
                }
                admin.setClassInfo(classInfo);
                arrayList.add(admin);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}