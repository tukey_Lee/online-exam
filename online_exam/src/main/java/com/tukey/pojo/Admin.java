package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * 用户实体
 */

@Data
@Component
//@Scope("prototype")
public class Admin {

    private Integer adminId;
    private String account;
    private String name;
    private String password;
    private Integer identity;
    private ClassInfo classInfo;

    public Admin() {
    }

    public Admin(Integer adminId, String account, String name, String password, Integer identity, ClassInfo classId) {
        this.adminId = adminId;
        this.account = account;
        this.name = name;
        this.password = password;
        this.identity = identity;
        this.classInfo = classId;
    }
}
