package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * 班级实体
 */


@Data
@Component
//@Scope("prototype")
public class ClassInfo {
    private int classId;
    private String className;

    public ClassInfo() {
    }

    public ClassInfo(int classId, String className) {
        this.classId = classId;
        this.className = className;
    }
}
