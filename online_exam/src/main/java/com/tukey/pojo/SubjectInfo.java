package com.tukey.pojo;


import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 试题实体
 */

@Data
@Component
@Scope("prototype")
//@SuppressWarnings("serial")
public class SubjectInfo implements Serializable {

    private Integer subjectId;
    private String subjectName;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String rightResult;
    private int subjectScore;
    private String subjectType;
    private int subjectEasy;
    private int division;
    private CourseInfo course;
    private ClassInfo classInfo;

    public SubjectInfo() {
    }

    public SubjectInfo(Integer subjectId, String subjectName, String optionA, String optionB, String optionC, String optionD, String rightResult, int subjectScore, String subjectType, int subjectEasy, int division, CourseInfo course, ClassInfo classInfo) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.optionA = optionA;
        this.optionB = optionB;
        this.optionC = optionC;
        this.optionD = optionD;
        this.rightResult = rightResult;
        this.subjectScore = subjectScore;
        this.subjectType = subjectType;
        this.subjectEasy = subjectEasy;
        this.division = division;
        this.course = course;
        this.classInfo = classInfo;
    }
}
