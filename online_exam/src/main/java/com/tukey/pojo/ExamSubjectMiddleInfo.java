package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * 试卷、试题中间关联表
 */
@Data
@Component
@Scope("prototype")
public class ExamSubjectMiddleInfo {

    private Integer esmId;
    private ExamPaperInfo examPaper;
    private SubjectInfo subject;

    public ExamSubjectMiddleInfo() {
    }

    public ExamSubjectMiddleInfo(Integer esmId, ExamPaperInfo examPaper, SubjectInfo subject) {
        this.esmId = esmId;
        this.examPaper = examPaper;
        this.subject = subject;
    }
}
