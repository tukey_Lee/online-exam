package com.tukey.pojo;


import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *考试安排实体
 */

@Data
@Component
@Scope("prototype")
public class ExamPlanInfo {

    private Integer examPlanId;
    private CourseInfo course;
    private ClassInfo classInfo;
    private ExamPaperInfo examPaper;
    private String beginTime;

    public ExamPlanInfo() {
    }

    public ExamPlanInfo(Integer examPlanId, CourseInfo course, ClassInfo classInfo, ExamPaperInfo examPaper, String beginTime) {
        this.examPlanId = examPlanId;
        this.course = course;
        this.classInfo = classInfo;
        this.examPaper = examPaper;
        this.beginTime = beginTime;
    }
}
