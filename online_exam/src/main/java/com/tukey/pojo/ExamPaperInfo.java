package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * 试卷实体
 */


@Data
@Component
@Scope("prototype")
public class ExamPaperInfo {

    private Integer examPaperId;
    private String examPaperName;
    private int subjectNum;
    private int examPaperTime;
    private int examPaperScore;
    private int division;
    private int examPaperEasy;
    private ClassInfo classInfo;

    public ExamPaperInfo() {
    }
    public ExamPaperInfo(Integer examPaperId, String examPaperName, int subjectNum, int examPaperTime, int examPaperScore, int division, int examPaperEasy, ClassInfo classInfo) {
        this.examPaperId = examPaperId;
        this.examPaperName = examPaperName;
        this.subjectNum = subjectNum;
        this.examPaperTime = examPaperTime;
        this.examPaperScore = examPaperScore;
        this.division = division;
        this.examPaperEasy = examPaperEasy;
        this.classInfo = classInfo;
    }
}
