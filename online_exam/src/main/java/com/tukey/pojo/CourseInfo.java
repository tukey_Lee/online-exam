package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 课程实体
 */


@Data
@Component
@Scope("prototype")
public class CourseInfo {

    private Integer courseId;
    private String courseName;
    private Integer division;
    private ClassInfo classInfo;

    public CourseInfo() {
    }

    public CourseInfo(Integer courseId, String courseName, Integer division, ClassInfo classInfo) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.division = division;
        this.classInfo = classInfo;
    }
}
