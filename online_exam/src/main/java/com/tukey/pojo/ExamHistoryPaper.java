package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 扩展类，用于封装历史考试信息、试卷信息、考试时间
 */


@Data
@Component
@Scope("prototype")
public class ExamHistoryPaper {

    // 试卷得分
    private int examScore;
    // 考试时间
    private String beginTime;

    private Integer examPaperId;
    private String examPaperName;
    private int subjectNum;
    private int examPaperScore;

    public ExamHistoryPaper() {
    }

    public ExamHistoryPaper(int examScore, String beginTime, Integer examPaperId, String examPaperName, int subjectNum, int examPaperScore) {
        this.examScore = examScore;
        this.beginTime = beginTime;
        this.examPaperId = examPaperId;
        this.examPaperName = examPaperName;
        this.subjectNum = subjectNum;
        this.examPaperScore = examPaperScore;
    }
}
