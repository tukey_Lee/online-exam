package com.tukey.pojo;

import org.springframework.stereotype.Component;

/**
 * 学生考试信息  主要用于前台图表绘制 数据封装
 */

@Component
public class StudentExamInfo {

    private Integer studentId;
    private String studentName;
    private Integer examSum;
    private Integer avgScore;

    public StudentExamInfo() {
    }

    public StudentExamInfo(Integer studentId, String studentName, Integer examSum, Integer avgScore) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.examSum = examSum;
        this.avgScore = avgScore;
    }
}
