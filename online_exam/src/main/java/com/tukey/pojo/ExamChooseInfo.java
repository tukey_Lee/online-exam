package com.tukey.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope("prototype")
public class ExamChooseInfo {

    private Integer chooseId;
    private Admin admin;
    private ExamPaperInfo examPaper;
    private SubjectInfo subject;
    private String chooseResult;

    public ExamChooseInfo() {
    }

    public ExamChooseInfo(Integer chooseId, Admin admin, ExamPaperInfo examPaper, SubjectInfo subject, String chooseResult) {
        this.chooseId = chooseId;
        this.admin = admin;
        this.examPaper = examPaper;
        this.subject = subject;
        this.chooseResult = chooseResult;
    }
}
