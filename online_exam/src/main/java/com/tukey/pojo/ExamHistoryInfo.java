package com.tukey.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 *历史考试
 *
 */

@Data
@Component
public class ExamHistoryInfo {

    private Integer historyId;
    private Admin admin;
    private ExamPaperInfo examPaper;
    private int examScore;

    public ExamHistoryInfo() {
    }

    public ExamHistoryInfo(Integer historyId, Admin admin, ExamPaperInfo examPaper, int examScore) {
        this.historyId = historyId;
        this.admin = admin;
        this.examPaper = examPaper;
        this.examScore = examScore;
    }
}
