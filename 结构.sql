/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : online_exam

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2022-06-07 14:34:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `account` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '账户',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `identity` int(6) NOT NULL COMMENT '身份',
  `classId` int(11) NOT NULL COMMENT '班级ID',
  PRIMARY KEY (`adminId`),
  KEY `classId` (`classId`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户信息表';

-- ----------------------------
-- Table structure for classinfo
-- ----------------------------
DROP TABLE IF EXISTS `classinfo`;
CREATE TABLE `classinfo` (
  `classId` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级Ida ',
  `className` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '班级',
  PRIMARY KEY (`classId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='班级管理表';

-- ----------------------------
-- Table structure for courseinfo
-- ----------------------------
DROP TABLE IF EXISTS `courseinfo`;
CREATE TABLE `courseinfo` (
  `courseId` int(11) NOT NULL COMMENT '课程ID',
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '课程名字',
  `division` int(11) DEFAULT NULL COMMENT '划分',
  `classId` int(11) DEFAULT NULL COMMENT '班级ID',
  PRIMARY KEY (`courseId`),
  KEY `classId` (`classId`) USING BTREE,
  CONSTRAINT `courseinfo_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='课程信息表';

-- ----------------------------
-- Table structure for examchooseinfo
-- ----------------------------
DROP TABLE IF EXISTS `examchooseinfo`;
CREATE TABLE `examchooseinfo` (
  `chooseId` int(11) NOT NULL AUTO_INCREMENT COMMENT '选择ID',
  `adminId` int(11) NOT NULL COMMENT '用户ID',
  `examPaperId` int(11) NOT NULL COMMENT '试卷ID',
  `subjectId` int(11) NOT NULL COMMENT '科目ID',
  `chooseResult` varchar(500) NOT NULL COMMENT '选择结果',
  PRIMARY KEY (`chooseId`),
  KEY `adminId` (`adminId`) USING BTREE,
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  KEY `subjectId` (`subjectId`) USING BTREE,
  CONSTRAINT `examchooseinfo_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `admin` (`adminId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examchooseinfo_ibfk_2` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examchooseinfo_ibfk_3` FOREIGN KEY (`subjectId`) REFERENCES `subjectinfo` (`subjectId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='选择答案信息管理';

-- ----------------------------
-- Table structure for examhistoryinfo
-- ----------------------------
DROP TABLE IF EXISTS `examhistoryinfo`;
CREATE TABLE `examhistoryinfo` (
  `historyId` int(11) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `examPaperId` int(11) NOT NULL,
  `examScore` int(11) DEFAULT NULL,
  PRIMARY KEY (`historyId`),
  KEY `adminId` (`adminId`) USING BTREE,
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  CONSTRAINT `examhistoryinfo_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `admin` (`adminId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examhistoryinfo_ibfk_2` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='历史考试信息管理';

-- ----------------------------
-- Table structure for exampaperinfo
-- ----------------------------
DROP TABLE IF EXISTS `exampaperinfo`;
CREATE TABLE `exampaperinfo` (
  `examPaperId` int(11) NOT NULL AUTO_INCREMENT COMMENT '试卷Id',
  `examPaperName` varchar(50) NOT NULL COMMENT '试卷名称',
  `subjectNum` int(11) NOT NULL COMMENT '考题数目',
  `examPaperTime` int(11) NOT NULL COMMENT '考试时长',
  `examPaperScore` int(11) NOT NULL,
  `classId` int(11) NOT NULL COMMENT '班级',
  `division` int(11) DEFAULT NULL COMMENT '划分',
  `examPaperEasy` int(11) DEFAULT NULL,
  PRIMARY KEY (`examPaperId`),
  KEY `classId` (`classId`) USING BTREE,
  KEY `examPaperTime` (`examPaperTime`),
  CONSTRAINT `exampaperinfo_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='试卷信息';

-- ----------------------------
-- Table structure for examplaninfo
-- ----------------------------
DROP TABLE IF EXISTS `examplaninfo`;
CREATE TABLE `examplaninfo` (
  `examPlanId` int(11) NOT NULL AUTO_INCREMENT COMMENT '考试安排ID',
  `courseId` int(11) NOT NULL COMMENT '课程ID',
  `classId` int(11) NOT NULL COMMENT '班级ID',
  `examPaperId` int(11) NOT NULL COMMENT '试卷ID',
  `beginTime` datetime NOT NULL COMMENT '考试开始时间',
  PRIMARY KEY (`examPlanId`),
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  KEY `examplaninfo_ibfk_3` (`classId`),
  KEY `examplaninfo_ibfk_5` (`courseId`),
  CONSTRAINT `examplaninfo_ibfk_3` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `examplaninfo_ibfk_4` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `examplaninfo_ibfk_5` FOREIGN KEY (`courseId`) REFERENCES `courseinfo` (`courseId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='考试安排表';

-- ----------------------------
-- Table structure for examsubjectmiddleinfo
-- ----------------------------
DROP TABLE IF EXISTS `examsubjectmiddleinfo`;
CREATE TABLE `examsubjectmiddleinfo` (
  `esmId` int(11) NOT NULL AUTO_INCREMENT,
  `examPaperId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  PRIMARY KEY (`esmId`),
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  KEY `subjectId` (`subjectId`) USING BTREE,
  CONSTRAINT `examsubjectmiddleinfo_ibfk_2` FOREIGN KEY (`subjectId`) REFERENCES `subjectinfo` (`subjectId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examsubjectmiddleinfo_ibfk_3` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='试卷、试题关联表';

-- ----------------------------
-- Table structure for subjectinfo
-- ----------------------------
DROP TABLE IF EXISTS `subjectinfo`;
CREATE TABLE `subjectinfo` (
  `subjectId` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(500) COLLATE utf8_estonian_ci NOT NULL,
  `optionA` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `optionB` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `optionC` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `optionD` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `rightResult` varchar(500) COLLATE utf8_estonian_ci NOT NULL,
  `subjectScore` int(11) NOT NULL,
  `subjectType` varchar(11) COLLATE utf8_estonian_ci NOT NULL,
  `courseId` int(11) NOT NULL,
  `classId` int(11) DEFAULT NULL,
  `subjectEasy` int(11) DEFAULT NULL,
  `division` int(11) NOT NULL,
  PRIMARY KEY (`subjectId`),
  KEY `courseId` (`courseId`) USING BTREE,
  KEY `classId` (`classId`) USING BTREE,
  CONSTRAINT `subjectinfo_ibfk_1` FOREIGN KEY (`courseId`) REFERENCES `courseinfo` (`courseId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `subjectinfo_ibfk_2` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=20010 DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci ROW_FORMAT=DYNAMIC COMMENT='试题信息管理';
