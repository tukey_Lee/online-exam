/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : online_exam

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2022-06-07 14:34:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `account` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '账户',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `identity` int(6) NOT NULL COMMENT '身份',
  `classId` int(11) NOT NULL COMMENT '班级ID',
  PRIMARY KEY (`adminId`),
  KEY `classId` (`classId`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户信息表';

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', '2022318001', 'lcm', 'admin', '1', '1');
INSERT INTO `admin` VALUES ('2', '2022318101', 'mzl', '1001', '2', '1');
INSERT INTO `admin` VALUES ('3', '2022318102', 'yjj', '1002', '2', '3');
INSERT INTO `admin` VALUES ('4', '2022318103', 'pyj', '1003', '2', '1');
INSERT INTO `admin` VALUES ('5', '2022318104', 'sp', '1004', '2', '1');
INSERT INTO `admin` VALUES ('6', '2022318105', 'wj', '1005', '2', '1');
INSERT INTO `admin` VALUES ('8', '2022318107', 'gse', '1007', '2', '2');
INSERT INTO `admin` VALUES ('9', '2022318108', 'gr', '1008', '2', '2');
INSERT INTO `admin` VALUES ('10', '2022318109', 'hydrh', '1009', '2', '3');
INSERT INTO `admin` VALUES ('11', '2022318110', 'erh', '1010', '2', '3');
INSERT INTO `admin` VALUES ('16', '2022318002', 'rzh', 'admin', '1', '1');
INSERT INTO `admin` VALUES ('17', '2022318003', 'srh', 'admin', '1', '2');
INSERT INTO `admin` VALUES ('18', '2022318004', 'rhz', 'admin', '1', '2');
INSERT INTO `admin` VALUES ('19', '2022318005', 'grfi', 'admin', '1', '3');
INSERT INTO `admin` VALUES ('21', '20223181011', 'gse2', '1011', '2', '2');
INSERT INTO `admin` VALUES ('22', '20223181012', 'gr2', '1012', '2', '2');
INSERT INTO `admin` VALUES ('23', '20223181013', 'hydrh2', '1013', '2', '3');
INSERT INTO `admin` VALUES ('24', '20223181114', 'erh2', '1014', '2', '3');

-- ----------------------------
-- Table structure for classinfo
-- ----------------------------
DROP TABLE IF EXISTS `classinfo`;
CREATE TABLE `classinfo` (
  `classId` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级Ida ',
  `className` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '班级',
  PRIMARY KEY (`classId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='班级管理表';

-- ----------------------------
-- Records of classinfo
-- ----------------------------
INSERT INTO `classinfo` VALUES ('1', '计算机科学与技术181');
INSERT INTO `classinfo` VALUES ('2', '信息技术与管理181');
INSERT INTO `classinfo` VALUES ('3', '网络工程181');

-- ----------------------------
-- Table structure for courseinfo
-- ----------------------------
DROP TABLE IF EXISTS `courseinfo`;
CREATE TABLE `courseinfo` (
  `courseId` int(11) NOT NULL COMMENT '课程ID',
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '课程名字',
  `division` int(11) DEFAULT NULL COMMENT '划分',
  `classId` int(11) DEFAULT NULL COMMENT '班级ID',
  PRIMARY KEY (`courseId`),
  KEY `classId` (`classId`) USING BTREE,
  CONSTRAINT `courseinfo_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='课程信息表';

-- ----------------------------
-- Records of courseinfo
-- ----------------------------
INSERT INTO `courseinfo` VALUES ('1', '计算机网络', '1', '1');
INSERT INTO `courseinfo` VALUES ('2', '数据结构', '2', '3');
INSERT INTO `courseinfo` VALUES ('3', '数据库', '2', '2');
INSERT INTO `courseinfo` VALUES ('4', '数据结构', '2', '1');

-- ----------------------------
-- Table structure for examchooseinfo
-- ----------------------------
DROP TABLE IF EXISTS `examchooseinfo`;
CREATE TABLE `examchooseinfo` (
  `chooseId` int(11) NOT NULL AUTO_INCREMENT COMMENT '选择ID',
  `adminId` int(11) NOT NULL COMMENT '用户ID',
  `examPaperId` int(11) NOT NULL COMMENT '试卷ID',
  `subjectId` int(11) NOT NULL COMMENT '科目ID',
  `chooseResult` varchar(500) NOT NULL COMMENT '选择结果',
  PRIMARY KEY (`chooseId`),
  KEY `adminId` (`adminId`) USING BTREE,
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  KEY `subjectId` (`subjectId`) USING BTREE,
  CONSTRAINT `examchooseinfo_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `admin` (`adminId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examchooseinfo_ibfk_2` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examchooseinfo_ibfk_3` FOREIGN KEY (`subjectId`) REFERENCES `subjectinfo` (`subjectId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='选择答案信息管理';

-- ----------------------------
-- Records of examchooseinfo
-- ----------------------------
INSERT INTO `examchooseinfo` VALUES ('1', '2', '1', '10000', '将IP地址翻译为计算机名、解析计算机的MAC地址');
INSERT INTO `examchooseinfo` VALUES ('2', '2', '1', '10001', 'UDP');
INSERT INTO `examchooseinfo` VALUES ('3', '2', '1', '10003', 'nslookup');
INSERT INTO `examchooseinfo` VALUES ('4', '2', '1', '10004', 'DhcpOffer');
INSERT INTO `examchooseinfo` VALUES ('5', '2', '1', '10005', '169.254.12.42');
INSERT INTO `examchooseinfo` VALUES ('6', '2', '1', '10006', '网关，掩码，浏览器，FTP');
INSERT INTO `examchooseinfo` VALUES ('7', '2', '1', '10007', '本地缓存记录→区域记录→转发域名服务器→根域名服务器');
INSERT INTO `examchooseinfo` VALUES ('8', '2', '1', '20000', '语法：数据与控制信息的结构或格式 语义：需要发出何种控制信息，完成何种动作做');
INSERT INTO `examchooseinfo` VALUES ('9', '2', '1', '20001', '协议是“水平”的，即协议是控制对等实体之间通信的规则 服务是“垂直”的，即服务是由下层向');
INSERT INTO `examchooseinfo` VALUES ('10', '2', '1', '10008', '64');
INSERT INTO `examchooseinfo` VALUES ('11', '2', '1', '10009', 'TCP协议的低开销特性');
INSERT INTO `examchooseinfo` VALUES ('12', '2', '1', '10010', 'IP地址');

-- ----------------------------
-- Table structure for examhistoryinfo
-- ----------------------------
DROP TABLE IF EXISTS `examhistoryinfo`;
CREATE TABLE `examhistoryinfo` (
  `historyId` int(11) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `examPaperId` int(11) NOT NULL,
  `examScore` int(11) DEFAULT NULL,
  PRIMARY KEY (`historyId`),
  KEY `adminId` (`adminId`) USING BTREE,
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  CONSTRAINT `examhistoryinfo_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `admin` (`adminId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examhistoryinfo_ibfk_2` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='历史考试信息管理';

-- ----------------------------
-- Records of examhistoryinfo
-- ----------------------------
INSERT INTO `examhistoryinfo` VALUES ('1', '2', '1', '18');
INSERT INTO `examhistoryinfo` VALUES ('2', '2', '1', '19');

-- ----------------------------
-- Table structure for exampaperinfo
-- ----------------------------
DROP TABLE IF EXISTS `exampaperinfo`;
CREATE TABLE `exampaperinfo` (
  `examPaperId` int(11) NOT NULL AUTO_INCREMENT COMMENT '试卷Id',
  `examPaperName` varchar(50) NOT NULL COMMENT '试卷名称',
  `subjectNum` int(11) NOT NULL COMMENT '考题数目',
  `examPaperTime` int(11) NOT NULL COMMENT '考试时长',
  `examPaperScore` int(11) NOT NULL,
  `classId` int(11) NOT NULL COMMENT '班级',
  `division` int(11) DEFAULT NULL COMMENT '划分',
  `examPaperEasy` int(11) DEFAULT NULL,
  PRIMARY KEY (`examPaperId`),
  KEY `classId` (`classId`) USING BTREE,
  KEY `examPaperTime` (`examPaperTime`),
  CONSTRAINT `exampaperinfo_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='试卷信息';

-- ----------------------------
-- Records of exampaperinfo
-- ----------------------------
INSERT INTO `exampaperinfo` VALUES ('1', '计算机网络', '18', '90', '100', '1', '0', '0');
INSERT INTO `exampaperinfo` VALUES ('2', '数据结构期末考试', '4', '90', '24', '1', '1', '1');
INSERT INTO `exampaperinfo` VALUES ('7', '测试', '6', '90', '36', '1', '0', '0');

-- ----------------------------
-- Table structure for examplaninfo
-- ----------------------------
DROP TABLE IF EXISTS `examplaninfo`;
CREATE TABLE `examplaninfo` (
  `examPlanId` int(11) NOT NULL AUTO_INCREMENT COMMENT '考试安排ID',
  `courseId` int(11) NOT NULL COMMENT '课程ID',
  `classId` int(11) NOT NULL COMMENT '班级ID',
  `examPaperId` int(11) NOT NULL COMMENT '试卷ID',
  `beginTime` datetime NOT NULL COMMENT '考试开始时间',
  PRIMARY KEY (`examPlanId`),
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  KEY `examplaninfo_ibfk_3` (`classId`),
  KEY `examplaninfo_ibfk_5` (`courseId`),
  CONSTRAINT `examplaninfo_ibfk_3` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `examplaninfo_ibfk_4` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `examplaninfo_ibfk_5` FOREIGN KEY (`courseId`) REFERENCES `courseinfo` (`courseId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='考试安排表';

-- ----------------------------
-- Records of examplaninfo
-- ----------------------------
INSERT INTO `examplaninfo` VALUES ('1', '1', '1', '1', '2022-04-10 00:00:00');

-- ----------------------------
-- Table structure for examsubjectmiddleinfo
-- ----------------------------
DROP TABLE IF EXISTS `examsubjectmiddleinfo`;
CREATE TABLE `examsubjectmiddleinfo` (
  `esmId` int(11) NOT NULL AUTO_INCREMENT,
  `examPaperId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  PRIMARY KEY (`esmId`),
  KEY `examPaperId` (`examPaperId`) USING BTREE,
  KEY `subjectId` (`subjectId`) USING BTREE,
  CONSTRAINT `examsubjectmiddleinfo_ibfk_2` FOREIGN KEY (`subjectId`) REFERENCES `subjectinfo` (`subjectId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examsubjectmiddleinfo_ibfk_3` FOREIGN KEY (`examPaperId`) REFERENCES `exampaperinfo` (`examPaperId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='试卷、试题关联表';

-- ----------------------------
-- Records of examsubjectmiddleinfo
-- ----------------------------
INSERT INTO `examsubjectmiddleinfo` VALUES ('1', '1', '10000');
INSERT INTO `examsubjectmiddleinfo` VALUES ('2', '1', '10001');
INSERT INTO `examsubjectmiddleinfo` VALUES ('3', '1', '10003');
INSERT INTO `examsubjectmiddleinfo` VALUES ('4', '1', '10004');
INSERT INTO `examsubjectmiddleinfo` VALUES ('5', '1', '10005');
INSERT INTO `examsubjectmiddleinfo` VALUES ('6', '1', '10006');
INSERT INTO `examsubjectmiddleinfo` VALUES ('7', '1', '10007');
INSERT INTO `examsubjectmiddleinfo` VALUES ('8', '1', '10008');
INSERT INTO `examsubjectmiddleinfo` VALUES ('9', '1', '10009');
INSERT INTO `examsubjectmiddleinfo` VALUES ('10', '1', '10010');
INSERT INTO `examsubjectmiddleinfo` VALUES ('12', '1', '20000');
INSERT INTO `examsubjectmiddleinfo` VALUES ('13', '1', '20001');
INSERT INTO `examsubjectmiddleinfo` VALUES ('14', '1', '20002');
INSERT INTO `examsubjectmiddleinfo` VALUES ('15', '1', '20003');
INSERT INTO `examsubjectmiddleinfo` VALUES ('16', '1', '20004');
INSERT INTO `examsubjectmiddleinfo` VALUES ('17', '1', '20005');
INSERT INTO `examsubjectmiddleinfo` VALUES ('18', '1', '20006');
INSERT INTO `examsubjectmiddleinfo` VALUES ('19', '1', '20007');
INSERT INTO `examsubjectmiddleinfo` VALUES ('24', '2', '10000');
INSERT INTO `examsubjectmiddleinfo` VALUES ('25', '2', '10005');
INSERT INTO `examsubjectmiddleinfo` VALUES ('26', '2', '20008');
INSERT INTO `examsubjectmiddleinfo` VALUES ('27', '2', '20009');
INSERT INTO `examsubjectmiddleinfo` VALUES ('34', '7', '10000');
INSERT INTO `examsubjectmiddleinfo` VALUES ('35', '7', '10001');
INSERT INTO `examsubjectmiddleinfo` VALUES ('36', '7', '10003');
INSERT INTO `examsubjectmiddleinfo` VALUES ('37', '7', '20001');
INSERT INTO `examsubjectmiddleinfo` VALUES ('38', '7', '20003');
INSERT INTO `examsubjectmiddleinfo` VALUES ('39', '7', '20004');

-- ----------------------------
-- Table structure for subjectinfo
-- ----------------------------
DROP TABLE IF EXISTS `subjectinfo`;
CREATE TABLE `subjectinfo` (
  `subjectId` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(500) COLLATE utf8_estonian_ci NOT NULL,
  `optionA` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `optionB` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `optionC` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `optionD` varchar(500) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `rightResult` varchar(500) COLLATE utf8_estonian_ci NOT NULL,
  `subjectScore` int(11) NOT NULL,
  `subjectType` varchar(11) COLLATE utf8_estonian_ci NOT NULL,
  `courseId` int(11) NOT NULL,
  `classId` int(11) DEFAULT NULL,
  `subjectEasy` int(11) DEFAULT NULL,
  `division` int(11) NOT NULL,
  PRIMARY KEY (`subjectId`),
  KEY `courseId` (`courseId`) USING BTREE,
  KEY `classId` (`classId`) USING BTREE,
  CONSTRAINT `subjectinfo_ibfk_1` FOREIGN KEY (`courseId`) REFERENCES `courseinfo` (`courseId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `subjectinfo_ibfk_2` FOREIGN KEY (`classId`) REFERENCES `classinfo` (`classId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=20010 DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci ROW_FORMAT=DYNAMIC COMMENT='试题信息管理';

-- ----------------------------
-- Records of subjectinfo
-- ----------------------------
INSERT INTO `subjectinfo` VALUES ('10000', 'DNS 服务器和DHCP服务器的作用是（）', '将IP地址翻译为计算机名，为客户机分配IP地址', '将IP地址翻译为计算机名、解析计算机的MAC地址', '将计算机名翻译为IP地址、为客户机分配IP地址', '将计算机名翻译为IP地址、解析计算机的MAC地址', '将计算机名翻译为IP地址、为客户机分配IP地址', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10001', 'HTTP协议通常使用什么协议进行传输（）', 'ARP', 'DHCP', 'UDP', 'TCP', 'TCP', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10003', '查看DNS缓存记录的命令（）', 'ipconfig/displaydns', 'nslookup', 'ipconfig/release', 'ipconfig/flushdns', 'ipconfig/displaydns', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10004', 'DHCP(        )报文的目的IP地址为255.255.255.255', 'DhcpDisover', 'DhcpOffer', 'DhcpAck', 'DhcpNack', 'DhcpDisover', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10005', '下列地址中，（  ）不是DHCP服务器分配的IP地址', '196.254.109.100', '169.254.12.42', '69.254.48.45', '96.254.54.15', '169.254.12.42', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10006', 'DHCP通常可以为客户端自动配置哪些网络参数（）', 'IP，掩码，网关，DNS', 'IP，掩码，域名，SMTP', '网关，掩码，浏览器，FTP', 'IP，网关，DNS，服务器', 'IP，掩码，网关，DNS', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10007', 'DNS服务器在名称解析过程中正确的查询顺序为（）', '本地缓存记录→区域记录→转发域名服务器→根域名服务器', '区域记录→本地缓存记录→转发域名服务器→根域名服务器', '本地缓存记录→区域记录→根域名服务器→转发域名服务器', '区域记录→本地缓存记录→根域名服务器→转发域名服务器', '本地缓存记录→区域记录→转发域名服务器→根域名服务器', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10008', '在TCP/IP协议中，序号小于（  ）的端口称为熟知端口(well-known port)。', '1024', '64', '256', '128', '1024', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10009', '在Internet上用TCP/IP播放视频，想用传输层的最快协议，以减少时延，要使用（ ）', 'UDP协议的低开销特性', 'UDP协议的高开销特性', 'TCP协议的低开销特性', 'TCP协议的高开销特性', 'UDP协议的低开销特性', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10010', '在TCP协议中采用（ ）来区分不同的应用进程', '端口号', 'IP地址', '协议类型', 'MAC地址', '端口号', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10011', '可靠的传输协议中的“可靠”指的是（ ）', '使用面向连接的会话', '使用“尽力而为”的传输', '使用滑动窗口来维持可靠性', '使用确认重传机制来确保传输的数据不丢失', '使用确认重传机制来确保传输的数据不丢失', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10012', '假设拥塞窗口为50KB，接收窗口为80KB，TCP能够发送的最大字节数为（ ）', '50KB', '80KB', '130KB', '30KB', '50KB', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10013', '主机A向主机B发送一个（SYN=1，seq=2000）的TCP报文，期望与主机B建立连接，若主机B接受连接请求，则主机B发送的正确有TCP报文可能是（ ）', '（SYN=0,ACK=0,seq=2001,ack=2001）', '（SYN=1,ACK=1,seq=2000,ack=2000）', '（SYN=1,ACK=1,seq=2001,ack=2001）', '（SYN=0,ACK=1,seq=2000,ack=2000）', '（SYN=1,ACK=1,seq=2001,ack=2001）', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10014', '主机A向主机B连续发送了两个TCP报文段，其序号分别为70和100。试问： （1）第一个报文段携带了（）个字节的数据？', ' 70', '30', '100', '170', '30', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10015', 'PCM脉码调制的过程（ ）', '采样、量化、编码', '量化、编码、采样', '编码、量化、采样', '采样、编码、量化', '采样、量化、编码', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10016', '若某采用4相位调制的通信链路的数据传输速率为2400bps，则该链路的波特率为（）', '600Baud', '1200Baud', '4800Baud', '9600Baud', '1200Baud', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10017', '以下关于数据传输速率的描述中，错误的是( )', '数据传输速率表示每秒钟传输构成数据代码的二进制比特数', '对于二进制数据，数据传输速率为S=1/T (bps)', '常用的数据传输速率单位有: 1Mbps=1.024×106bps', '数据传输速率是描述数据传输系统性能的重要技术指标之一', '常用的数据传输速率单位有: 1Mbps=1.024×106bps', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10018', '以下关于时分多路复用概念的描述中，错误的是.(  ).', '时分多路复用将线路使用的时间分成多个时间片', '时分多路复用分为同步时分多路复用与统计时分多路复用', '时分多路复用使用“帧”与数据链路层“帧”的概念、作用是不同的', '统计时分多路复用将时间片预先分配给各个信道', '统计时分多路复用将时间片预先分配给各个信道', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10019', '1000BASE-T标准支持的传输介质是（）', '双绞线', '同轴电缆', '光纤', '无线电', '双绞线', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10020', '一个以太网交换机，读取整个数据帧，对数据帧进行差错校验后再转发出去，这种交换方式称为 （）', '直通交换', '无碎片交换', '无差错交换', '存储转发交换', '存储转发交换', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10021', '关于VLAN，下面的描述中正确的是（）', '一个新的交换机没有配置VLAN', '通过配置VLAN减少了冲突域的数量', '一个VLAN不能跨越多个交换机', '各个VLAN属于不同的广播域', '各个VLAN属于不同的广播域', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10022', '以太网协议中使用物理地址作用是什么？', '.用于不同子网中的主机进行通信', '作为第二层设备的唯一标识', '用于区别第二层第三层的协议数据单元', '保存主机可检测未知的远程设备', '作为第二层设备的唯一标识', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10023', '以太网采用的CSMA/CD协议，当冲突发生时要通过二进制指数后退算法计算后退延时， 关于这个算法，以下论述中错误的是 （）', '冲突次数越多，后退的时间越短', '平均后退次数的多少与负载大小有关', '后退时延的平均值与负载大小有关', '重发次数达到一定极限后放弃发送', '冲突次数越多，后退的时间越短', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10024', '以下关于交换机获取与其端口连接设备的MAC地址的叙述中，正确的是（）', '交换机从路由表中提取设备的MAC地址', '交换机检查端口流入分组的源地址', '交换机之间互相交换地址表', '网络管理员手工输入设备的MAC地址', '交换机检查端口流入分组的源地址', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10025', '如果G (x）为11010010，以下4个CRC校验比特序列中只有哪个可能是正确的 ？', '1101011001', '101011011', '11011011', '1011001', '101011011', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10026', '以下关于Ethernet物理地址的描述中，错误的是', 'Ethernet物理地址又叫做MAC地址', '48位的Ethernet物理地址允许分配的地址数达到247个', '网卡的物理地址写入主机的EPROM中', '每一块网卡的物理地址在全世界是唯一的', '网卡的物理地址写入主机的EPROM中', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10027', '下列帧类型中，不属于HDLC帧类型的是（）', '信息帧', '确认帧', '监控帧', '无编号帧', '监控帧', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10028', '通过交换机连接的一组站点，关于它们的广播域和冲突域说法正确的是（）', '组成一个冲突域，但不是一个广播域', '组成一个广播域，但不是一个冲突域', '组成一个冲突域，也是一个广播域', '既不一个冲突域，也不是一个广播域', '组成一个广播域，但不是一个冲突域', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10029', '数据链路层的数据单位是（）', '帧', '字节', '比特', '分组', '帧', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10030', 'LAN参考模型可分为物理层、（ ）', 'MAC，LLC等三层', 'LLC，MHS等三层', 'MAC，FTAM等三层', 'LLC，VT等三层', 'MAC，LLC等三层', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10031', 'DNS 服务器和DHCP服务器的作用是（）', 'A', 'B', 'C', 'D', 'B', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('10032', '测试', '测试a', '测试b', '测试c', '测试d', '测试a', '2', '1', '1', '1', '2', '1');
INSERT INTO `subjectinfo` VALUES ('20000', '简述网络协议的三要素', null, null, null, null, '语法：数据与控制信息的结构或格式 语义：需要发出何种控制信息，完成何种动作做出何种响应\r\n 同步：事件实现顺序的说明', '10', '绪论', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20001', '简述“协议是水平的、服务是垂直的”含义', null, null, null, null, '协议是“水平”的，即协议是控制对等实体之间通信的规则 服务是“垂直”的，即服务是由下层向上层通过层间接口提供的', '10', '绪论', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20002', '物理层接口有哪几个方面的特性及其所包含的内容', null, null, null, null, '机械特性:说明接口所用的接线器的形状和尺寸、引线数目和排列、固定和锁定装置等等 电气特性:指明在接口电缆的各条线上出现的电压的范围 功能特性:指明某条线上出现的某一电平的电压表示何意 规程特性:说明对于不同功能的各种可能事件的出现顺序', '10', '物理层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20003', '为什么要使用信道复用技术、常用的信道复用技术', null, null, null, null, '通过共享信道、最大限度提高信道利用率。常用的信道复用技术有:频分、时分、码分、波分', '10', '物理层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20004', '简述网络适配器（网卡）的作用', null, null, null, null, '串并行转换 数据缓存 管理驱动 实现以太网驱动', '10', '数据链路层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20005', '简述RIP路由选择协议的主要特点', null, null, null, null, '只能用于不超过15个路由器的小型网络 仅和相邻路由器交换信息 路由器交换的所有信息是本路由器知道的全部信息 按固定的时间间隔交换路由信息', '10', '网络层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20006', '路由器的功能是什么', null, null, null, null, '路由选择 分组转发', '10', '网络层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20007', 'IP服务的特点', null, null, null, null, '非连接 尽最大努力交付 不可靠', '10', '网络层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20008', '简述ARP的工作原理', null, null, null, null, '工作原理：A先广播发送请求，B收到后在缓存中写入A的IP地址到硬件地址映射单播响应，然后在A的缓存中写入B的IP地址到硬件地址映射', '10', '网络层', '1', '1', '1', '2');
INSERT INTO `subjectinfo` VALUES ('20009', '测试', null, null, null, null, '测试', '10', '测试', '1', '1', '1', '2');
